﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {

	private int id;
	private string text;
	private int leben;
	private int angriff;
	private int energy;
	private int character;
	private bool selected;
	private bool isSpell;

	public bool IsSpell {
		get {
			return isSpell;
		}
		set {
			isSpell = value;
		}
	}

	public bool Selected {
		get { return selected; }
		set { selected = value; }
	}

	public int Id {
		get {
			return id;
		}
		set {
			id = value;
		}
	}
	
	public string Text {
		get {
			return text;
		}
		set {
			text = value;
		}
	}
	
	public int Leben {
		get {
			return leben;
		}
		set {
			leben = value;
		}
	}
	
	public int Angriff {
		get {
			return angriff;
		}
		set {
			angriff = value;
		}
	}
	
	public int Energy {
		get {
			return energy;
		}
		set {
			energy = value;
		}
	}
	
	public int Character {
		get {
			return character;
		}
		set {
			character = value;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
