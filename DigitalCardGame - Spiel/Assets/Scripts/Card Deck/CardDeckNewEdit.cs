﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CardDeckNewEdit : MonoBehaviour {

	public GameObject EditButton;

	public GameObject WaitingScreen;
	public GameObject Cancel;
	public GameObject GoAhead;

	Color initialColor;

	void Start () {
		initialColor = EditButton.GetComponent<Image> ().color;
		if (!Parameter.gotDeck) {
			Debug.Log ("Kein Deck vohanden!");
			EditButton.GetComponent<Button>().interactable = false;
			EditButton.GetComponent<Image> ().color = Color.grey;
		} else if (Parameter.gotDeck) {
			Debug.Log ("Deck vohanden!");
			EditButton.GetComponent<Button>().interactable = true;
			EditButton.GetComponent<Image> ().color = initialColor;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BackClicked () {
		Application.LoadLevel ("MainMenu");
	}

	public void NewClicked () {
		if (Parameter.gotDeck) {
			WaitingScreen.SetActive (true);
		} else {
			Parameter.NewOrEditCardDeck = false;
			Application.LoadLevel ("CardDeckCharacterChoice");
		}
	}

	public void ClickedCancel () {
		WaitingScreen.SetActive (false);
	}

	public void ClickedGoAhead () {
		Parameter.NewOrEditCardDeck = false;
		Application.LoadLevel ("CardDeckCharacterChoice");
	}

	public void ClickedEdit () {
		Parameter.NewOrEditCardDeck = true;
		Application.LoadLevel ("CardSelection");
	}
}
