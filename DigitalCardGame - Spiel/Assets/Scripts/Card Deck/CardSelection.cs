﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CardSelection : MonoBehaviour {

	public GameObject card1;
	public GameObject card2;
	public GameObject card3;
	public GameObject card4;
	public GameObject card5;

	public GameObject moreCardsFurther;
	public GameObject moreCardsBack;
	public GameObject saveButton;

	public Sprite basicSprite;
	public Sprite basicSpriteAdded;
	public Sprite specialSprite1;
	public Sprite specialSprite1Added;
	public Sprite specialSprite2;
	public Sprite specialSprite2Added;
	public Sprite specialSprite3;
	public Sprite specialSprite3Added;
	public Sprite specialSprite4;
	public Sprite specialSprite4Added;
	public Sprite transparentSprite;

	public Sprite spellBasicSprite;
	public Sprite spellBasicSpriteAdded;
	public Sprite spellBasicSpriteDeleted;
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite1Added;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite2Added;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite3Added;
	public Sprite spellSpecialSprite4;
	public Sprite spellSpecialSprite4Added;

	public int siteCounter;
	public int basicCardsCounter;
	public int specialCardsCounter;

	public int basicCounter;
	public int specialCounter;

	public static bool specialArea;
	public bool SpecialArea { get; set; }

	public static bool basicArea;
	public bool BasicArea { get; set; }

	public static bool selectedArea;
	public bool SelectedArea { get; set; }

	public List<Card> allCards;

	public static List<Card> basicCards;
	public List<Card> BasicCards { get; set; }

	public static List<Card> specialCards;
	public List<Card> SpecialCards { get; set; }

	Component[] compCard1;
	Component[] compCard2;
	Component[] compCard3;
	Component[] compCard4;
	Component[] compCard5;

	GameObject[] cardObjects;

	public Color initialColor;

	public static List<Card> selectedCards;
	public List<Card> SelectedCards { get; set; }

	public static List<Card> backupCards;
	public List<Card> BackupCards { get; set; }

	public GameObject NoCardsSelected;

	public Text CardCounter;
	public Text SiteCounter;

	public GameObject WaitingScreen;
	public GameObject WaitingScreenButton;
	public Text WaitingScreenText;

	public GameObject specialButton;
	public GameObject basicButton;
	public GameObject choosenButton;

	public Color initialColorSBC;

	public GameObject RewardPanel;
	public GameObject RewardPanelButton;

	public GameObject loadAnimation;
	public Image loadImage;

	public GameObject DeactivatePanelAfterSaving;

	void Update() {
		loadImage.transform.Rotate(0.0f, 0.0f, -1.0f);
		if (Input.GetKeyDown (KeyCode.RightArrow) && moreCardsFurther.GetComponent<Button> ().interactable)
			ClickedFurther ();
		if (Input.GetKeyDown (KeyCode.LeftArrow) && moreCardsBack.GetComponent<Button> ().interactable)
			ClickedReturn ();
		if (Input.GetKeyDown (KeyCode.Keypad1))
			ClickedSpecial ();
		if (Input.GetKeyDown (KeyCode.Keypad2))
			ClickedBasic ();
		if (Input.GetKeyDown (KeyCode.Keypad3))
			ClickedUsed ();
		if (selectedCards.Count >= 30) {
			saveButton.GetComponent<Image> ().color = initialColor;
			saveButton.GetComponent<Button> ().interactable = true;
		} else {
			saveButton.GetComponent<Image> ().color = Color.grey;
			saveButton.GetComponent<Button> ().interactable = false;
		}
		CardCounter.text = selectedCards.Count + "/30";
		int counter = 0;
		if (basicArea) {
			counter = basicCards.Count/5;
			basicButton.GetComponent<Image> ().color = initialColorSBC;
			choosenButton.GetComponent<Image> ().color = Color.grey;
			specialButton.GetComponent<Image> ().color = Color.grey;
		} else if (specialArea) {
			counter = specialCards.Count/5;
			specialButton.GetComponent<Image> ().color = initialColorSBC;
			basicButton.GetComponent<Image> ().color = Color.grey;
			choosenButton.GetComponent<Image> ().color = Color.grey;
		} else if (selectedArea) {
			counter = selectedCards.Count/5;
			choosenButton.GetComponent<Image> ().color = initialColorSBC;
			basicButton.GetComponent<Image> ().color = Color.grey;
			specialButton.GetComponent<Image> ().color = Color.grey;
		}
		counter++;
		int siteCounterDisplay = siteCounter + 1;
		if (basicArea) {
			if (basicCards.Count % 5 == 0) {
				SiteCounter.text = siteCounterDisplay + "/" + basicCards.Count/5;
			} else {
				SiteCounter.text = siteCounterDisplay + "/" + counter;
			}
		}
		if (specialArea) {
			if (specialCards.Count % 5 == 0) {
				SiteCounter.text = siteCounterDisplay + "/" + (specialCards.Count/5);
			} else {
				SiteCounter.text = siteCounterDisplay + "/" + counter;
			}
		}
		if (selectedCards.Count <= 5 && selectedArea) {
			moreCardsFurther.GetComponent<Button> ().interactable = false;
			moreCardsFurther.GetComponent<Image> ().color = Color.grey;
		}
		if (selectedArea) {
			if (selectedCards.Count % 5 == 0) {
				SiteCounter.text = siteCounterDisplay + "/" + (selectedCards.Count/5);
			} else {
				SiteCounter.text = siteCounterDisplay + "/" + counter;
			}
		}
		if (selectedCards.Count == 0 && selectedArea) {
			SiteCounter.text = "0/0";
		}
	}

	void Start () {
		Parameter.spellBasicSprite = spellBasicSprite;
		Parameter.spellBasicSpriteAdded = spellBasicSpriteAdded;
		Parameter.spellBasicSpriteDeleted = spellBasicSpriteDeleted;
		RewardPanelButton.GetComponent<Image> ().color = Color.grey;
		RewardPanelButton.GetComponent<Button> ().interactable = false;
		RewardPanel.SetActive(false);
		cardObjects = new GameObject[] {card1, card2, card3, card4, card5};
		BoxCollider bc;
		foreach (var cardObject in cardObjects) {
			bc = cardObject.AddComponent<BoxCollider>();
			bc.size = new Vector3(142.8f, 204.8f,1);
		}
		initialColorSBC = basicButton.GetComponent<Image> ().color;
		initialColor = moreCardsFurther.GetComponent<Image>().color;
		moreCardsBack.GetComponent<Image> ().color = Color.grey;
		moreCardsFurther.GetComponent<Image> ().color = initialColor;
		moreCardsBack.GetComponent<Button> ().interactable = false;
		moreCardsFurther.GetComponent<Button> ().interactable = true;
		saveButton.GetComponent<Image> ().color = Color.grey;
		saveButton.GetComponent<Button> ().interactable = false;

		if (!Parameter.NewOrEditCardDeck) {
			foreach (var card in LoadAllCards.allCards) {
				card.Selected = false;
			}
			specialArea = true;
			basicArea = false;
			selectedArea = false;
		} else if (Parameter.NewOrEditCardDeck) {
			foreach (var card in LoadAllCards.allCards) {
				foreach (var deckCard in LoadCardDeck.deckCards) {
					if (deckCard.Id == card.Id) {
						card.Selected = true;
					}
				}
			}
			specialArea = false;
			basicArea = false;
			selectedArea = true;
		}
		SendData ();
	}

	public void InitCards (List<Card> specialCards, int specialCounter) {
		FillCards (specialCards, specialCounter, 5*siteCounter);
	}

	public void ClickedUsed () {
		siteCounter = 0;
		basicArea = false;
		specialArea = false;
		selectedArea = true;
		ResetComponents ();
		if (selectedCards.Count != 0) {
			FillCards (selectedCards, selectedCards.Count, 5 * siteCounter);
			if (selectedCards.Count > 5 * siteCounter) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
				moreCardsBack.GetComponent<Button> ().interactable = false;
				moreCardsBack.GetComponent<Image> ().color = Color.grey;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
				moreCardsBack.GetComponent<Button> ().interactable = false;
				moreCardsBack.GetComponent<Image> ().color = Color.grey;
			}
		} else {
			ResetComponents ();
			NoCardsSelected.GetComponent<Text>().text = "Du hast noch keine Karten ausgewählt!";
			moreCardsFurther.GetComponent<Button> ().interactable = false;
			moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			moreCardsBack.GetComponent<Button> ().interactable = false;
			moreCardsBack.GetComponent<Image> ().color = Color.grey;
		}
	}
	
	public void ClickedBasic () {
		NoCardsSelected.GetComponent<Text> ().text = "";
		basicArea = true;
		specialArea = false;
		selectedArea = false;
		siteCounter = 0;
		specialCardsCounter = 5;
		basicCardsCounter = 5;
		ResetComponents ();
		FillCards (basicCards, basicCounter, basicCardsCounter*siteCounter);
		if (basicCounter > 5*(siteCounter + 1)) {
			moreCardsFurther.GetComponent<Button> ().interactable = true;
			moreCardsFurther.GetComponent<Image> ().color = initialColor;
			moreCardsBack.GetComponent<Button> ().interactable = false;
			moreCardsBack.GetComponent<Image> ().color = Color.grey;
		}
	}

	public void ClickedFurther () {
		ResetComponents ();
		siteCounter++;
		if (basicArea && basicCards.Count > basicCardsCounter*siteCounter) {
			FillCards (basicCards, basicCards.Count, basicCardsCounter*siteCounter);
			if (basicCards.Count > basicCardsCounter*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		} else if (specialArea && specialCards.Count > specialCardsCounter*siteCounter) {
			FillCards (specialCards, specialCards.Count, specialCardsCounter*siteCounter);
			if (specialCards.Count > specialCardsCounter*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		} else if (selectedArea && selectedCards.Count > 5*siteCounter) {
			FillCards(selectedCards, selectedCards.Count, 5*siteCounter);
			if (selectedCards.Count > 5*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		}
		if (siteCounter > 0) {
			moreCardsBack.GetComponent<Button> ().interactable = true;
			moreCardsBack.GetComponent<Image> ().color = initialColor;
		} else {
			moreCardsBack.GetComponent<Button> ().interactable = true;
			moreCardsBack.GetComponent<Image> ().color = initialColor;
		}
	}

	public void ClickedReturn () {
		siteCounter--;
		ResetComponents ();
		if (siteCounter == 0) {
			moreCardsBack.GetComponent<Button> ().interactable = false;
			moreCardsBack.GetComponent<Image> ().color = Color.gray;
			moreCardsFurther.GetComponent<Button> ().interactable = true;
			moreCardsFurther.GetComponent<Image> ().color = initialColor;
		} else {
			moreCardsBack.GetComponent<Button> ().interactable = true;
			moreCardsBack.GetComponent<Image> ().color = initialColor;
		}
		if (basicArea && basicCards.Count > basicCardsCounter*siteCounter) {
			FillCards (basicCards, basicCards.Count, basicCardsCounter*siteCounter);
			if (basicCards.Count > basicCardsCounter*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		} else if (specialArea && specialCards.Count > specialCardsCounter*siteCounter) {
			FillCards (specialCards, specialCards.Count, specialCardsCounter*siteCounter);
			if (specialCards.Count > specialCardsCounter*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		} else if (selectedArea && selectedCards.Count > 5*siteCounter) {
			FillCards(selectedCards, selectedCards.Count, 5*siteCounter);
			if (selectedCards.Count > 5*(siteCounter + 1)) {
				moreCardsFurther.GetComponent<Button> ().interactable = true;
				moreCardsFurther.GetComponent<Image> ().color = initialColor;
			} else {
				moreCardsFurther.GetComponent<Button> ().interactable = false;
				moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			}
		}
	}

	public void ClickedSpecial () {
		NoCardsSelected.GetComponent<Text> ().text = "";
		specialArea = true;
		basicArea = false;
		selectedArea = false;
		siteCounter = 0;
		basicCardsCounter = 5;
		specialCardsCounter = 5;
		ResetComponents ();
		FillCards (specialCards, specialCounter, specialCardsCounter*siteCounter);
		if (specialCounter > 5 * siteCounter) {
			moreCardsFurther.GetComponent<Button> ().interactable = true;
			moreCardsFurther.GetComponent<Image> ().color = initialColor;
			moreCardsBack.GetComponent<Button> ().interactable = false;
			moreCardsBack.GetComponent<Image> ().color = Color.grey;
		} else {
			moreCardsFurther.GetComponent<Button> ().interactable = false;
			moreCardsFurther.GetComponent<Image> ().color = Color.grey;
			moreCardsBack.GetComponent<Button> ().interactable = false;
			moreCardsBack.GetComponent<Image> ().color = Color.grey;
		}
	}

	public void FillCards (List<Card> cards, int amountOfCards, int counter) 
	{	
		Image image;
		int cardsToDisplay = 0;	
		string splitText1 = null;
		string splitText2 = null;
		string splitText3 = null; 
		string splitText4 = null; 
		string splitText5 = null; 

		// Erhöht - Fügt - Zieht - Erhöht
		if (amountOfCards > counter) {
			splitText1 = cards [counter].IsSpell.ToString();
			Debug.Log (cards[counter].IsSpell);
			if (cards[counter].IsSpell) {

				compCard1 [0].GetComponent<Text> ().text = "";
				compCard1 [1].GetComponent<Text> ().text = "";
			} else {
				compCard1 [0].GetComponent<Text> ().text = cards [counter].Leben.ToString ();
				compCard1 [1].GetComponent<Text> ().text = cards [counter].Angriff.ToString ();
			}
			compCard1 [2].GetComponent<Text> ().text = cards [counter].Energy.ToString ();
			compCard1 [3].GetComponent<Text> ().text = cards [counter].Text.ToString ();
			compCard1 [4].GetComponent<Text> ().text = cards [counter].Id.ToString ();
			compCard1 [5].GetComponent<Text> ().text = cards [counter].Character.ToString ();
			cardsToDisplay++;
		}

		if (amountOfCards > counter + 1) {
			splitText2 = cards [counter + 1].IsSpell.ToString();
			Debug.Log (cards[counter+1].IsSpell);
			if (cards[counter+1].IsSpell) {
				compCard2 [0].GetComponent<Text> ().text = "";
				compCard2 [1].GetComponent<Text> ().text = "";
			} else {
				compCard2 [0].GetComponent<Text> ().text = cards [counter + 1].Leben.ToString ();
				compCard2 [1].GetComponent<Text> ().text = cards [counter + 1].Angriff.ToString ();
			}
			compCard2 [2].GetComponent<Text> ().text = cards [counter +1].Energy.ToString ();
			compCard2 [3].GetComponent<Text> ().text = cards [counter +1].Text.ToString ();
			compCard2 [4].GetComponent<Text> ().text = cards [counter +1].Id.ToString ();
			compCard2 [5].GetComponent<Text> ().text = cards [counter +1].Character.ToString ();
			cardsToDisplay++;
		}

		if (amountOfCards > counter +2) {
			splitText3 = cards [counter + 2].IsSpell.ToString();
			if (cards[counter+2].IsSpell) {
				compCard3 [0].GetComponent<Text> ().text = "";
				compCard3 [1].GetComponent<Text> ().text = "";
			} else {
				compCard3 [0].GetComponent<Text> ().text = cards [counter + 2].Leben.ToString ();
				compCard3 [1].GetComponent<Text> ().text = cards [counter + 2].Angriff.ToString ();
			}
			compCard3 [2].GetComponent<Text> ().text = cards [counter +2].Energy.ToString ();
			compCard3 [3].GetComponent<Text> ().text = cards [counter +2].Text.ToString ();
			compCard3 [4].GetComponent<Text> ().text = cards [counter +2].Id.ToString ();
			compCard3 [5].GetComponent<Text> ().text = cards [counter +2].Character.ToString ();
			cardsToDisplay++;
		}

		if (amountOfCards > counter + 3) {
			splitText4 = cards [counter + 3].IsSpell.ToString();
			if (cards[counter+3].IsSpell) {
				compCard4 [0].GetComponent<Text> ().text = "";
				compCard4 [1].GetComponent<Text> ().text = "";
			} else {
				compCard4 [0].GetComponent<Text> ().text = cards [counter + 3].Leben.ToString ();
				compCard4 [1].GetComponent<Text> ().text = cards [counter + 3].Angriff.ToString ();
			}
			compCard4 [2].GetComponent<Text> ().text = cards [counter + 3].Energy.ToString ();
			compCard4 [3].GetComponent<Text> ().text = cards [counter + 3].Text.ToString ();
			compCard4 [4].GetComponent<Text> ().text = cards [counter + 3].Id.ToString ();
			compCard4 [5].GetComponent<Text> ().text = cards [counter + 3].Character.ToString ();
			cardsToDisplay++;
		}

		if (amountOfCards > counter +4) {
			splitText5 = cards [counter + 4].IsSpell.ToString();
			if (cards[counter+4].IsSpell) {
				compCard5 [0].GetComponent<Text> ().text = "";
				compCard5 [1].GetComponent<Text> ().text = "";
			} else {
				compCard5 [0].GetComponent<Text> ().text = cards [counter + 4].Leben.ToString ();
				compCard5 [1].GetComponent<Text> ().text = cards [counter + 4].Angriff.ToString ();
			}
			compCard5 [2].GetComponent<Text> ().text = cards [counter +4].Energy.ToString ();
			compCard5 [3].GetComponent<Text> ().text = cards [counter +4].Text.ToString ();
			compCard5 [4].GetComponent<Text> ().text = cards [counter +4].Id.ToString ();
			compCard5 [5].GetComponent<Text> ().text = cards [counter +4].Character.ToString ();
			cardsToDisplay++;
		}

		if (basicArea) {
			if (cardsToDisplay >= 1 ) {
				image = card1.GetComponent<Image> ();
				//image.sprite = basicSprite;
				ChooseBasicSprite (image, splitText1);
			} else {
				image = card1.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 2 ) {
				image = card2.GetComponent<Image> ();
				//image.sprite = basicSprite;
				ChooseBasicSprite (image, splitText2);
			} else {
				image = card2.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 3 ) {
				image = card3.GetComponent<Image> ();
				//image.sprite = basicSprite;
				ChooseBasicSprite (image, splitText3);
			} else {
				image = card3.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 4 ) {
				image = card4.GetComponent<Image> ();
				//image.sprite = basicSprite;
				ChooseBasicSprite (image, splitText4);
			} else {
				image = card4.GetComponent<Image> ();
				image.sprite = transparentSprite;

			}
			if (cardsToDisplay == 5 ) {
				image = card5.GetComponent<Image> ();
				//image.sprite = basicSprite;
				ChooseBasicSprite (image, splitText5);
			} else {
				image = card5.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
		} else if (specialArea) {
			if (cardsToDisplay >= 1 ) {
				image = card1.GetComponent<Image> ();
				ChooseSpecialCardFromCharacter (image, splitText1);
			} else {
				image = card1.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 2 ) {
				image = card2.GetComponent<Image> ();
				ChooseSpecialCardFromCharacter (image, splitText2);
			} else {
				image = card2.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 3 ) {
				image = card3.GetComponent<Image> ();
				ChooseSpecialCardFromCharacter (image, splitText3);
			} else {
				image = card3.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay >= 4 ) {
				image = card4.GetComponent<Image> ();
				ChooseSpecialCardFromCharacter (image, splitText4);
			} else {
				image = card4.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
			if (cardsToDisplay == 5 ) {
				image = card5.GetComponent<Image> ();
				ChooseSpecialCardFromCharacter (image, splitText5);
			} else {
				image = card5.GetComponent<Image> ();
				image.sprite = transparentSprite;
			}
		}
		if (selectedArea) {
			if (cardsToDisplay >= 1) {
				if (compCard1 [5].GetComponent<Text> ().text != "0") {
					image = card1.GetComponent<Image> ();
					ChooseSpecialCardFromCharacter (image, splitText1);
				} else {
					image = card1.GetComponent<Image> ();
					ChooseBasicSprite (image, splitText1);
				}
			}
			if (cardsToDisplay >= 2 ) {
				if (compCard2 [5].GetComponent<Text> ().text != "0") {
					image = card2.GetComponent<Image> ();
					ChooseSpecialCardFromCharacter (image, splitText2);
				} else {
					image = card2.GetComponent<Image> ();
					ChooseBasicSprite (image, splitText2);
				}
			}
			if (cardsToDisplay >= 3 ) {
				if (compCard3 [5].GetComponent<Text> ().text != "0") {
					image = card3.GetComponent<Image> ();
					ChooseSpecialCardFromCharacter (image, splitText3);
				} else {
					image = card3.GetComponent<Image> ();
					ChooseBasicSprite (image, splitText3);
				}
			}
			if (cardsToDisplay >= 4 ) {
				if (compCard4 [5].GetComponent<Text> ().text != "0") {
					image = card4.GetComponent<Image> ();
					ChooseSpecialCardFromCharacter (image, splitText4);
				} else {
					image = card4.GetComponent<Image> ();
					ChooseBasicSprite (image, splitText4);
				}
			}
			if (cardsToDisplay == 5 ) {
				if (compCard5 [5].GetComponent<Text> ().text != "0") {
					image = card5.GetComponent<Image> ();
					ChooseSpecialCardFromCharacter (image, splitText5);
				} else {
					image = card5.GetComponent<Image> ();
					ChooseBasicSprite (image, splitText5);
				}
			}
		}

		int i = counter;
		if (!selectedArea) {
			foreach (var cardObject in cardObjects) {
				if (i < cards.Count) {
					if (cards[i].Selected) {
						FillSpriteLikeCharacter (cardObject, cards[i].IsSpell);
					}
				}
				i++;
			}
		}
		i = 0;
		cardsToDisplay = 0;
	}

	public void FillSpriteLikeCharacter (GameObject card, bool isSpell) {
		Image image;
		if (specialArea) {
			image = card.GetComponent<Image> ();
			if (Parameter.characterId == "1") {
				if (isSpell) {
					image.sprite = spellSpecialSprite1Added;
				} else {
					image.sprite = specialSprite1Added;
				}
			} else if (Parameter.characterId == "2") {
				if (isSpell) {
					image.sprite = spellSpecialSprite2Added;
				} else {
					image.sprite = specialSprite2Added;
				}
			} else if (Parameter.characterId == "3") {
				if (isSpell) {
					image.sprite = spellSpecialSprite3Added;
				} else {
					image.sprite = specialSprite3Added;
				}
			} else if (Parameter.characterId == "4") {
				if (isSpell) {
					image.sprite = spellSpecialSprite4Added;
				} else {
					image.sprite = specialSprite4Added;
				}
			}
		} else if (basicArea) {
			image = card.GetComponent<Image> ();
			if (isSpell) {
				image.sprite = spellBasicSpriteAdded;
			} else {
				image.sprite = basicSpriteAdded;
			}
		}
	}

	public void ChooseBasicSprite(Image image, string text) {
		bool isSpell = false;
		if (text == "True") {
			isSpell = true;
		}
		if (isSpell) {
			image.sprite = spellBasicSprite;
		} else {
			image.sprite = basicSprite;
		}
	}


	public void ChooseSpecialCardFromCharacter (Image image, string text) {
		bool isSpell = false;
		if (text == "True") {
			isSpell = true;
		}
		if (Parameter.characterId == "0") {
			if (isSpell) {
				image.sprite = spellBasicSprite;
			} else {
				image.sprite = basicSprite;
			}
		} else if (Parameter.characterId == "1") {
			if (isSpell) {
				image.sprite = spellSpecialSprite1;
			} else {
				image.sprite = specialSprite1;
			}
		} else if (Parameter.characterId == "2") {
			if (isSpell) {
				image.sprite = spellSpecialSprite2;
			} else {
				image.sprite = specialSprite2;
			}
		} else if (Parameter.characterId == "3") {
			if (isSpell) {
				image.sprite = spellSpecialSprite3;
			} else {
				image.sprite = specialSprite3;
			}
		} else if (Parameter.characterId == "4") {
			if (isSpell) {
				image.sprite = spellSpecialSprite4;
			} else {
				image.sprite = specialSprite4;
			}
		}
	}

	public void SendData() {

		allCards = new List<Card> ();
		basicCards = new List<Card>();
		specialCards = new List<Card>();
		selectedCards = new List<Card> ();

		specialCardsCounter = 5;
		basicCardsCounter = 5;

		compCard1 = cardObjects[0].GetComponentsInChildren<Text> ();
		compCard2 = cardObjects[1].GetComponentsInChildren<Text> ();
		compCard3 = cardObjects[2].GetComponentsInChildren<Text> ();
		compCard4 = cardObjects[3].GetComponentsInChildren<Text> ();
		compCard5 = cardObjects[4].GetComponentsInChildren<Text> ();

		siteCounter = 0;

		if (Parameter.NewOrEditCardDeck) {
			foreach (var card in Parameter.deckCards) {
				if (card.Character != 0) {
					Parameter.characterId = card.Character.ToString ();
				}
			}
		}

		foreach (var card in LoadAllCards.allCards) {
			if (card.Character == System.Int32.Parse(Parameter.characterId)) {
				specialCards.Add(card);
			} else if (card.Character == 0) {
				basicCards.Add(card);
			}
		}

		basicCounter = basicCards.Count;
		specialCounter = specialCards.Count;

		backupCards = new List<Card> ();

		if (!Parameter.NewOrEditCardDeck) {
			basicArea = false;
			specialArea = true;
			selectedArea = false;	
			InitCards (specialCards, specialCounter);
		} else if (Parameter.NewOrEditCardDeck) {
			basicArea = false;
			specialArea = false;
			selectedArea = true;
			selectedCards = Parameter.deckCards;
			backupCards.AddRange(selectedCards);
			InitCards (selectedCards, selectedCards.Count);
		}
	}

	public void ClickedBack () {
		selectedCards.Clear();
		selectedCards.AddRange(backupCards);
		if (Parameter.NewOrEditCardDeck) {
			Application.LoadLevel ("CardDeckNewEdit");
		} else {
			Application.LoadLevel ("CardDeckCharacterChoice");
		}
	}

	public void ResetComponents () {
		Image image;
		compCard1 [0].GetComponent<Text> ().text = "";
		compCard1 [1].GetComponent<Text> ().text = "";
		compCard1 [2].GetComponent<Text> ().text = "";
		compCard1 [3].GetComponent<Text> ().text = "";
		compCard2 [0].GetComponent<Text> ().text = "";
		compCard2 [1].GetComponent<Text> ().text = "";
		compCard2 [2].GetComponent<Text> ().text = "";
		compCard2 [3].GetComponent<Text> ().text = "";
		compCard3 [0].GetComponent<Text> ().text = "";
		compCard3 [1].GetComponent<Text> ().text = "";
		compCard3 [2].GetComponent<Text> ().text = "";
		compCard3 [3].GetComponent<Text> ().text = "";
		compCard4 [0].GetComponent<Text> ().text = "";
		compCard4 [1].GetComponent<Text> ().text = "";
		compCard4 [2].GetComponent<Text> ().text = "";
		compCard4 [3].GetComponent<Text> ().text = "";
		compCard5 [0].GetComponent<Text> ().text = "";
		compCard5 [1].GetComponent<Text> ().text = "";
		compCard5 [2].GetComponent<Text> ().text = "";
		compCard5 [3].GetComponent<Text> ().text = "";
		image = card1.GetComponent<Image> ();
		image.sprite = transparentSprite;
		image = card2.GetComponent<Image> ();
		image.sprite = transparentSprite;
		image = card3.GetComponent<Image> ();
		image.sprite = transparentSprite;
		image = card4.GetComponent<Image> ();
		image.sprite = transparentSprite;
		image = card5.GetComponent<Image> ();
		image.sprite = transparentSprite;

		image = card1.GetComponent<Image> ();
		image.color = initialColor;
		image = card2.GetComponent<Image> ();
		image.color = initialColor;
		image = card3.GetComponent<Image> ();
		image.color = initialColor;
		image = card4.GetComponent<Image> ();
		image.color = initialColor;
		image = card5.GetComponent<Image> ();
		image.color = initialColor;

		saveButton.GetComponent<Image> ().color = Color.grey;
		saveButton.GetComponent<Button> ().interactable = false;
	}

	public void ClickedSave() {
		DeactivatePanelAfterSaving.SetActive (false);
		WaitingScreen.SetActive(true);
		WaitingScreenButton.SetActive(false);
		card1.SetActive (false);
		card2.SetActive (false);
		card3.SetActive (false);
		card4.SetActive (false);
		card5.SetActive (false);
		if (selectedCards.Count >= 30) {
			loadAnimation.SetActive(true);
			WaitingScreenButton.SetActive(false);
			WaitingScreenText.text = "Dein Deck wird gespeichert!";
			string DECK_IDS = "";
			foreach (var card in selectedCards) {
				DECK_IDS = DECK_IDS + card.Id.ToString() + ";";
			}
			if (Parameter.deckCards == null) {
				StartCoroutine(SendDeck (Parameter.Username, DECK_IDS,false));
			} else {
				StartCoroutine(SendDeck (Parameter.Username, DECK_IDS,true));
			}
			LoadCardDeck.deckCards = selectedCards;
			Parameter.deckCards = selectedCards;
			Parameter.gotDeck = true;	
			foreach (var card in selectedCards) {
				if (card.Character != 0) {
					GameParameter.characterID = card.Character;
				}
			}
		} else {
			card1.SetActive (true);
			card2.SetActive (true);
			card3.SetActive (true);
			card4.SetActive (true);
			card5.SetActive (true);
		}
	}
	
	public IEnumerator SendDeck(string username, string Deck_IDS, bool check) {
		string saveUrl = "http://digitalcardgame.bplaced.net/save_deck.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("Deck_IDS", Deck_IDS);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			loadAnimation.SetActive(false);
			WaitingScreenButton.SetActive(true);
			WaitingScreenText.text = "Dein Deck konnte nicht gespeichert werden!";
		} else {
			if (www.text != "False") {
				if (check == false) {
					WaitingScreen.SetActive(false);
					RewardPanel.SetActive(true);
					//WaitingScreenText.text = "Dein erstes Deck wurde soeben gespeichert! Du erhälst 10 Coins und 50 Erfahrungspunkte!";
					StartCoroutine(SendCoins (Parameter.Username, Parameter.Coins + 10, Parameter.Levelpoints + 50));
				} else {
					WaitingScreenText.text = "Dein Deck wurde gespeichert!";
					loadAnimation.SetActive(false);
					WaitingScreenButton.SetActive(true);
				}
			}
			www.Dispose ();
		}
	}

	public void ClickedAcceptReward() {
		Application.LoadLevel ("MainMenu");
	}

	public IEnumerator SendCoins(string username, int coins, int poins) {
		string saveUrl = "http://digitalcardgame.bplaced.net/save_coins.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("coins", coins);
		form.AddField ("poins", poins);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
		} else {
			if (www.text != "False") {
				Parameter.deckCards = selectedCards;
			}
			www.Dispose ();
		}
		RewardPanelButton.GetComponent<Image> ().color = initialColor;
		RewardPanelButton.GetComponent<Button> ().interactable = true;
	}

	public void ClickedReady () {
		Application.LoadLevel ("MainMenu");
	}
}
