﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HighlightCard : MonoBehaviour {
	
	private Vector3 originalScale;
	public Color initialColor;
	public GameObject card;
	Component[] compCard;

	public Sprite basicSprite;
	public Sprite basicSpriteAdded;
	public Sprite basicSpriteDeleted;
	public Sprite specialSprite1;
	public Sprite specialSprite1Added;
	public Sprite specialSprite1Deleted;
	public Sprite specialSprite2;
	public Sprite specialSprite2Added;
	public Sprite specialSprite2Deleted;
	public Sprite specialSprite3;
	public Sprite specialSprite3Added;
	public Sprite specialSprite3Deleted;
	public Sprite specialSprite4;
	public Sprite specialSprite4Added;
	public Sprite specialSprite4Deleted;
	public Sprite transparentSprite;

	public Sprite spellBasicSprite;
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite1Added;
	public Sprite spellSpecialSprite1Deleted;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite2Added;
	public Sprite spellSpecialSprite2Deleted;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite3Added;
	public Sprite spellSpecialSprite3Deleted;
	public Sprite spellSpecialSprite4;
	public Sprite spellSpecialSprite4Added;
	public Sprite spellSpecialSprite4Deleted;

	public bool removed;
	public bool added;

	//public GameObject infoText;
	
	// Use this for initialization
	void Start () {
		
	}
	
	void OnMouseEnter () {
		originalScale = transform.localScale;
		transform.localScale = new Vector3 (1.18f, 1.18f, 1.18f);
		initialColor = card.GetComponent<Image> ().color;
		card.GetComponent<Image> ().color = Color.green;
	}
	
	void OnMouseExit () {
		card.GetComponent<Image> ().color = initialColor;
		transform.localScale = originalScale;
		//infoText.SetActive (false);
	}
	
	void OnMouseDown () {
		if (card.GetComponent<Image> ().sprite != transparentSprite) {
			compCard = card.GetComponentsInChildren<Text> ();
			Card selectedCard = gameObject.AddComponent<Card> ();
			selectedCard.Id = System.Int32.Parse (compCard [4].GetComponent<Text> ().text);
			if (CardSelection.selectedArea) {
				if (!Parameter.NewOrEditCardDeck) {
					CardSelection.backupCards.Clear();
					CardSelection.backupCards.AddRange(CardSelection.selectedCards);
				}
				foreach (var compare in CardSelection.backupCards) {
					if (selectedCard.Id == compare.Id) {
						if (compare.Selected) {
							CardSelection.selectedCards.RemoveAll (o => o.Id == selectedCard.Id);
							FillSpriteLikeCharacter (true, compare);
							compare.Selected = false;
						} else if (!compare.Selected) {
							if (CardSelection.selectedCards.Count < 30) {
								CardSelection.selectedCards.Add (compare);
								FillSpriteLikeCharacter (false, compare);
								compare.Selected = true;
							}
						}
					}
				}
			} else {
				if (CardSelection.basicArea) {
					foreach (var compare in CardSelection.basicCards) {
						if (selectedCard.Id == compare.Id) {
							if (compare.Selected) {
								CardSelection.selectedCards.RemoveAll (o => o.Id == selectedCard.Id);
								FillSpriteLikeCharacter (false, compare);
								compare.Selected = false;
							} else if (!compare.Selected) {
								if (CardSelection.selectedCards.Count < 30) {
									CardSelection.selectedCards.Add (compare);
									FillSpriteLikeCharacter (true, compare);
									compare.Selected = true;
								}
							}
						}
					}
				} else if (CardSelection.specialArea) {
					foreach (var compare in CardSelection.specialCards) {
						if (selectedCard.Id == compare.Id) {
							if (compare.Selected) {
								CardSelection.selectedCards.RemoveAll (o => o.Id == selectedCard.Id);
								FillSpriteLikeCharacter (false, compare);
								compare.Selected = false;
							} else if (!compare.Selected) {
								if (CardSelection.selectedCards.Count < 30) {
									CardSelection.selectedCards.Add (compare);
									FillSpriteLikeCharacter (true, compare);
									compare.Selected = true;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void FillSpriteLikeCharacter (bool check, Card playableCard) {
		Image image;
		image = card.GetComponent<Image> ();
		if (check) {
			if (CardSelection.selectedArea) {
				image = card.GetComponent<Image> ();
				if (playableCard.Character == 1) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite1Deleted;
					} else {
						image.sprite = specialSprite1Deleted;
					}
				} else if (playableCard.Character == 2) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite2Deleted;
					} else {
						image.sprite = specialSprite2Deleted;
					}
				} else if (playableCard.Character == 3) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite3Deleted;
					} else {
						image.sprite = specialSprite3Deleted;
					}
				} else if (playableCard.Character == 4) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite4Deleted;
					} else {
						image.sprite = specialSprite4Deleted;
					}
				} else {
					image = card.GetComponent<Image> ();
					if (playableCard.IsSpell) {
						image.sprite = Parameter.spellBasicSpriteDeleted;
					} else {
						image.sprite = basicSpriteDeleted;
					}
				}
			} else {
				image = card.GetComponent<Image> ();
				if (playableCard.Character == 1) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite1Added;
					} else {
						image.sprite = specialSprite1Added;
					}
				} else if (playableCard.Character == 2) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite2Added;
					} else {
						image.sprite = specialSprite2Added;
					}
				} else if (playableCard.Character == 3) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite3Added;
					} else {
						image.sprite = specialSprite3Added;
					}
				} else if (playableCard.Character == 4) {
					if (playableCard.IsSpell) {
						image.sprite = spellSpecialSprite4Added;
					} else {
						image.sprite = specialSprite4Added;
					}
				} else {
					image = card.GetComponent<Image> ();
					if (playableCard.IsSpell) {
						image.sprite = Parameter.spellBasicSpriteAdded;
					} else {
						image.sprite = basicSpriteAdded;
					}
				}
			}
		} else if (!check) {
			image = card.GetComponent<Image> ();
			if (playableCard.Character == 1) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite1;
				} else {
					image.sprite = specialSprite1;
				}
			} else if (playableCard.Character == 2) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite2;
				} else {
					image.sprite = specialSprite2;
				}
			} else if (playableCard.Character == 3) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite3;
				} else {
					image.sprite = specialSprite3;
				}
			} else if (playableCard.Character == 4) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite4;
				} else {
					image.sprite = specialSprite4;
				}
			} else {
				image = card.GetComponent<Image> ();
				if (playableCard.IsSpell) {
					image.sprite = spellBasicSprite;
				} else {
					image.sprite = basicSprite;
				}
			}
		}
	}
}
