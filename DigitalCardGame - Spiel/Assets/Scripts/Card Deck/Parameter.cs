﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Parameter : MonoBehaviour {
	
	public static string characterId;
	
	public static string Username;
	public static int Coins;
	public static int Level;
	public static int Levelpoints;
	public static int GamesWon;
	public static int GamesLost;
	
	public static List<Card> deckCards;
	public static List<Card> allCards;
	public static List<Card> allCardsForShop;
	
	public static List<Card> playableCards;
	
	public static bool gotDeck;
	
	/// <summary>
	/// If new -> false
	/// If edit -> true
	/// </summary>
	public static bool NewOrEditCardDeck;
	
	public const string registeredGameName = "DCG"; 
	
	public static List<string> myCards;

	public static Text coinsPanel;

	public static Sprite spellBasicSprite;
	public static Sprite spellBasicSpriteAdded;
	public static Sprite spellBasicSpriteDeleted;
}
