﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class LoadCardDeck : MonoBehaviour {
	public static List<Card> deckCards;
	public GameObject CardDeckButton;
	public GameObject PlayGameButton;
	public GameObject Notification;
	public GameObject ShopButton;
	public GameObject NocardDeckFoundNotification;
	public Color initialColor;
	public Color initialColorCardDeckButton;
	public Color newColor;

	// Use this for initialization
	void Start () {
		NocardDeckFoundNotification.SetActive (false);
		initialColor = PlayGameButton.GetComponent<Image>().color;
		initialColorCardDeckButton = CardDeckButton.GetComponent<Image>().color;
		CardDeckButton.GetComponent<Button>().interactable = false;
		ShopButton.GetComponent<Button>().interactable = false;
		ShopButton.GetComponent<Image> ().color = Color.gray;
		//CardDeckButton.GetComponent<Image> ().color = newColor;
		PlayGameButton.GetComponent<Button>().interactable = false;
		PlayGameButton.GetComponent<Image> ().color = Color.grey;
		if (Parameter.deckCards == null) {
			StartCoroutine (GetDeck ());
		} else {
			CardDeckButton.GetComponent<Button>().interactable = true;
			CardDeckButton.GetComponent<Image>().color = initialColorCardDeckButton;
			PlayGameButton.GetComponent<Button>().interactable = true;
			PlayGameButton.GetComponent<Image>().color = initialColor;
			ShopButton.GetComponent<Button>().interactable = true;
			Notification.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () {
	}

	public IEnumerator GetDeck () {
		string saveUrl = "http://digitalcardgame.bplaced.net/get_deck.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", Parameter.Username);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				if (www.text == "") {
					Parameter.gotDeck = false;
					CardDeckButton.GetComponent<Button>().interactable = true;
					CardDeckButton.GetComponent<Image>().color = initialColorCardDeckButton;
					Notification.SetActive(false);
					NocardDeckFoundNotification.SetActive(true);
					return false;
				}
				Parameter.gotDeck = true;
				deckCards = new List<Card>();
				string[] act = www.text.Split (';');
				for (int i = 0; i < act.Length; i++) {
					foreach (var card in LoadAllCards.allCards) {
						if (card.Id.ToString() == act[i].ToString()) {
							deckCards.Add(card);
							if (card.Character != 0) {
								GameParameter.characterID = card.Character;
							}
						}
					}
				}
				CardDeckButton.GetComponent<Button>().interactable = true;
				CardDeckButton.GetComponent<Image>().color = initialColorCardDeckButton;
				PlayGameButton.GetComponent<Button>().interactable = true;
				PlayGameButton.GetComponent<Image>().color = initialColor;
				ShopButton.GetComponent<Button>().interactable = true;
				NocardDeckFoundNotification.SetActive(false);
			} else {
				Parameter.gotDeck = false;
				Notification.SetActive(false);
			}
			www.Dispose ();
			Parameter.deckCards = deckCards;
			Notification.SetActive(false);
			NocardDeckFoundNotification.SetActive(false);
		}
	}
}