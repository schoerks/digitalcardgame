﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.Linq.Expressions;

public class PackAnimation : MonoBehaviour {
	
	public GameObject bestCard;
	public GameObject otherCards;
	
	public GameObject card2;
	public GameObject card3;
	public GameObject card4;
	public GameObject card5;
	
	List<Card> cards;
	
	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;

	public Sprite spellBasicSprite;
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;
	
	public Sprite transparentSprite;
	
	Component[] compCard1 = null;
	Component[] compCard2 = null;
	Component[] compCard3 = null;
	Component[] compCard4 = null;
	Component[] compCard5 = null;
	
	GameObject[] cardObjects;
	
	public GameObject packsPanel;
	public GameObject packsPanelBackground;
	public GameObject pack1;
	public GameObject pack2;
	public Text desc;
	
	public Sprite packSprite;
	public Sprite packsPanelBackgroundSprite;
	
	public GameObject addAll;

	Color initialColor;
	public Text text;
	public Text packname;
	public Text coins;
	public Text anzahl;
	public GameObject coinsImage;
	bool selectedPack;

	public GameObject yes;
	public GameObject no;

	int discardPrice;
	
	// Use this for initialization
	void Start () {
		discardPrice = 0;
		selectedPack = true;
		GameParameter.selectedPack = false;
		//FillPacks ();
		initialColor = this.GetComponent<SpriteRenderer> ().color;
		this.GetComponent<SpriteRenderer> ().sprite = packSprite;
		selectedPack = false;
		packsPanelBackground.GetComponent<Image> ().sprite = packsPanelBackgroundSprite;
		bestCard.SetActive (true);
		otherCards.SetActive (true);
		packsPanel.SetActive (true);
		addAll.SetActive (false);
		cards = new List<Card>();
		cardObjects = new GameObject[] {bestCard, card2, card3, card4, card5};
		compCard1 = cardObjects[0].GetComponentsInChildren<Text> ();
		compCard2 = cardObjects[1].GetComponentsInChildren<Text> ();
		compCard3 = cardObjects[2].GetComponentsInChildren<Text> ();
		compCard4 = cardObjects[3].GetComponentsInChildren<Text> ();
		compCard5 = cardObjects[4].GetComponentsInChildren<Text> ();
		ClearAllCards ();
	}
	
	void OnMouseDown() {
		if (gameObject.name == "Pack1") {
			pack1.GetComponent<Animator> ().Play ("ChoosePack");
			pack2.GetComponent<Animator> ().Play ("Pack2Invisible");
		} else {
			pack1.GetComponent<Animator> ().Play ("Pack1Invisible");
			pack2.GetComponent<Animator> ().Play ("ChoosePack2");
		}
		yes.SetActive (true);
		no.SetActive (true);
		packname.text = "Pack kaufen?";
		GameParameter.selectedPack = true;
	}

	public void animatePackBack() {
		if (gameObject.name == "Pack1") {
			pack1.GetComponent<Animator> ().Play ("BackPack1");
			pack2.GetComponent<Animator> ().Play ("Pack2Visible");
		} else {
			pack2.GetComponent<Animator> ().Play ("BackPack2");
			pack1.GetComponent<Animator> ().Play ("Pack1Visible");
		}
		yes.SetActive (false);
		no.SetActive (false);
		packname.text = "Wähle ein Pack aus";
		GameParameter.selectedPack = false;
	}

	public void getCards() {
		if (Parameter.Coins >= 1000 && this.name == "Pack2") {
			packname.text = "";
			yes.SetActive(false);
			no.SetActive(false);
			SelectFiveRandomCards ();
			ClearPacks ();
			FillSelectedCards ();
			StartCoroutine (PlayPackAnimation ());
			StartCoroutine (SendCoins (Parameter.Username, Parameter.Coins - 1000, Parameter.Levelpoints, Parameter.GamesWon, Parameter.GamesLost));
			Parameter.Coins -= 1000;
			Parameter.coinsPanel.text = "" + Parameter.Coins;
		} else if (Parameter.Coins >= 1500) {
			packname.text = "";
			yes.SetActive(false);
			no.SetActive(false);
			SelectFiveRandomCards ();
			ClearPacks ();
			FillSelectedCards ();
			StartCoroutine (PlayPackAnimation ());
			StartCoroutine (SendCoins (Parameter.Username, Parameter.Coins - 1500 + discardPrice, Parameter.Levelpoints, Parameter.GamesWon, Parameter.GamesLost));
			Parameter.Coins -= (1500);
			Parameter.Coins += discardPrice;
			Parameter.coinsPanel.text = "" + Parameter.Coins;
		}
		//addAll.SetActive (true);
	}
	
	public IEnumerator SendCoins(string username, int c, int poins, int won, int lost) {
		string saveUrl = "http://digitalcardgame.bplaced.net/save_coins.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("coins", c);
		form.AddField ("poins", poins);
		form.AddField ("won", won);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				Debug.Log("Gespeichert");
				Parameter.Coins = c;
				Parameter.Levelpoints = poins;
			}
			www.Dispose ();
		}
	}
	
	
	/// <summary>
	/// Starts the pack animation
	/// </summary>
	/// <returns>The pack animation.</returns>
	IEnumerator PlayPackAnimation() {
		bestCard.GetComponent<Animator>().Play("BestCardOfPack");
		yield return new WaitForSeconds (2.8f);
		otherCards.GetComponent<Animator>().Play("OtherCardsAppear");
		//addAll.GetComponent<Animator> ().Play ("appear");
		yield return new WaitForSeconds (1f);
		packsPanel.SetActive (false);
	}
	
	/// <summary>
	/// Selects five random cards for
	/// a pack which the player buyed
	/// </summary>
	void SelectFiveRandomCards() {
		bool isGarantie = false;
		while (isGarantie == false) {
			for (int i = 0; i < 5; i++) {
				int x = Random.Range (0, Parameter.allCardsForShop.Count);
				Card c = Parameter.allCardsForShop [x];
				cards.Add (c);
			}
			List<Card> sortedList = cards.OrderByDescending (o => o.Energy).ToList ();
			cards = sortedList;
			if (sortedList[0].Energy >= 7) {
				isGarantie = true;
				
				Debug.Log ("Name: " + this.name);
			}
			if (this.name == "Pack1") {
				isGarantie = true;
			}
		}
		LoadNewCardsIntoDatabase ();
	}
	
	public void LoadNewCardsIntoDatabase() {
		string newCardsString = "";
		int counter = 0;
		while (counter < Parameter.myCards.Count-1) {
			newCardsString += Parameter.myCards[counter] + ",";
			counter++;
		}
		Debug.Log (newCardsString);
		int counterCards = 0;
		foreach (var newCard in cards) {
			bool isNew = true;
			foreach (var myCard in Parameter.myCards) {
				if (newCard.Id.ToString() == myCard) {
					isNew = false;
				}
			}
			if (isNew && counterCards < 5) {
				Parameter.myCards.Add(newCard.Id.ToString());
				Parameter.allCards.Add(newCard);
				LoadAllCards.allCards.Add(newCard);
				newCardsString += newCard.Id.ToString()+",";
				Debug.Log("NEW");
				cardObjects[counterCards].GetComponentsInChildren<Text> ()[7].text = "new";
			} else {
				string x = cardObjects[counterCards].GetComponentsInChildren<Text>()[0].text;
				Debug.Log("Amount:" +x);
				discardPrice += 107;
			}
			counterCards++;
		}
		Debug.Log (newCardsString);
		StartCoroutine(SetCardIDs (Parameter.Username, newCardsString));
	}
	
	public IEnumerator SetCardIDs(string username, string card_IDs) {
		string saveUrl = "http://digitalcardgame.bplaced.net/update_cardIDs.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("Card_IDs", card_IDs);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				Debug.Log("Gespeichert");
			}
			www.Dispose ();
		}
	}
	
	/// <summary>
	/// Fills the selected cards
	/// with the correct sprite.
	/// All correct values are added.
	/// </summary>
	void FillSelectedCards() {
		Image image;
		if (cards[0].IsSpell) {
			compCard1 [0].GetComponent<Text> ().text = "";
			compCard1 [1].GetComponent<Text> ().text = "";
		} else {
			compCard1 [0].GetComponent<Text> ().text = cards [0].Leben.ToString ();
			compCard1 [1].GetComponent<Text> ().text = cards [0].Angriff.ToString ();
		}
		compCard1 [2].GetComponent<Text> ().text = cards [0].Energy.ToString ();
		compCard1 [3].GetComponent<Text> ().text = cards [0].Text.ToString ();
		compCard1 [4].GetComponent<Text> ().text = cards [0].Id.ToString ();
		compCard1 [5].GetComponent<Text> ().text = cards [0].Character.ToString ();
		if (cards[1].IsSpell) {
			compCard2 [0].GetComponent<Text> ().text = "";
			compCard2 [1].GetComponent<Text> ().text = "";
		} else {
			compCard2 [0].GetComponent<Text> ().text = cards [1].Leben.ToString ();
			compCard2 [1].GetComponent<Text> ().text = cards [1].Angriff.ToString ();
		}
		compCard2 [2].GetComponent<Text> ().text = cards [1].Energy.ToString ();
		compCard2 [3].GetComponent<Text> ().text = cards [1].Text.ToString ();
		compCard2 [4].GetComponent<Text> ().text = cards [1].Id.ToString ();
		compCard2 [5].GetComponent<Text> ().text = cards [1].Character.ToString ();
		if (cards[2].IsSpell) {
			compCard3 [0].GetComponent<Text> ().text = "";
			compCard3 [1].GetComponent<Text> ().text = "";
		} else {
			compCard3 [0].GetComponent<Text> ().text = cards [2].Leben.ToString ();
			compCard3 [1].GetComponent<Text> ().text = cards [2].Angriff.ToString ();
		}
		compCard3 [2].GetComponent<Text> ().text = cards [2].Energy.ToString ();
		compCard3 [3].GetComponent<Text> ().text = cards [2].Text.ToString ();
		compCard3 [4].GetComponent<Text> ().text = cards [2].Id.ToString ();
		compCard3 [5].GetComponent<Text> ().text = cards [2].Character.ToString ();
		if (cards[3].IsSpell) {
			compCard4 [0].GetComponent<Text> ().text = "";
			compCard4 [1].GetComponent<Text> ().text = "";
		} else {
			compCard4 [0].GetComponent<Text> ().text = cards [3].Leben.ToString ();
			compCard4 [1].GetComponent<Text> ().text = cards [3].Angriff.ToString ();
		}
		compCard4 [2].GetComponent<Text> ().text = cards [3].Energy.ToString ();
		compCard4 [3].GetComponent<Text> ().text = cards [3].Text.ToString ();
		compCard4 [4].GetComponent<Text> ().text = cards [3].Id.ToString ();
		compCard4 [5].GetComponent<Text> ().text = cards [3].Character.ToString ();
		if (cards[4].IsSpell) {
			compCard5 [0].GetComponent<Text> ().text = "";
			compCard5 [1].GetComponent<Text> ().text = "";
		} else {
			compCard5 [0].GetComponent<Text> ().text = cards [4].Leben.ToString ();
			compCard5 [1].GetComponent<Text> ().text = cards [4].Angriff.ToString ();
		}
		compCard5 [2].GetComponent<Text> ().text = cards [4].Energy.ToString ();
		compCard5 [3].GetComponent<Text> ().text = cards [4].Text.ToString ();
		compCard5 [4].GetComponent<Text> ().text = cards [4].Id.ToString ();
		compCard5 [5].GetComponent<Text> ().text = cards [4].Character.ToString ();
		
		int i = 0;
		foreach (var cardObject in cardObjects) {
			if (cardObject.GetComponentsInChildren<Text> ()[7].text == "new") {
				cardObject.transform.FindChild("New").gameObject.SetActive(true);
			}
			image = cardObject.GetComponent<Image> ();
			if (cards[i].Character == 0) {
				if (cards[i].IsSpell) {
					image.sprite = spellBasicSprite;
				} else {
					image.sprite = basicSprite;
				}
			} else {
				ChooseSpecialCardFromCharacter (image, cards[i].Character, cards[i].IsSpell);
			}
			i++;
		}
	}
	
	/// <summary>
	/// Chooses the special card from character.
	/// </summary>
	/// <param name="image">Image.</param>
	/// <param name="characterId">Character identifier.</param>
	/// <param name="text">Text.</param>
	public void ChooseSpecialCardFromCharacter (Image image, int characterId, bool isSpell) {
		if (characterId == 0) {
			if (isSpell) {
				image.sprite = spellBasicSprite;
			} else {
				image.sprite = basicSprite;
			}
		} else if (characterId == 1) {
			if (isSpell) {
				image.sprite = spellSpecialSprite1;
			} else {
				image.sprite = specialSprite1;
			}
		} else if (characterId == 2) {
			if (isSpell) {
				image.sprite = spellSpecialSprite2;
			} else {
				image.sprite = specialSprite2;
			}
		} else if (characterId == 3) {
			if (isSpell) {
				image.sprite = spellSpecialSprite3;
			} else {
				image.sprite = specialSprite3;
			}
		} else if (characterId == 4) {
			if (isSpell) {
				image.sprite = spellSpecialSprite4;
			} else {
				image.sprite = specialSprite4;
			}
		}
	}
	
	void ClearPacks() {
		desc.text = "";
		pack1.GetComponent<SpriteRenderer> ().sprite = transparentSprite;
		pack2.GetComponent<SpriteRenderer> ().sprite = transparentSprite;
		packsPanelBackground.GetComponent<Image> ().sprite = transparentSprite;
	}
	
	/// <summary>
	/// Clears all sprites and values
	/// of the five cards.
	/// </summary>
	void ClearAllCards () {
		foreach (var item in cardObjects) {
			item.GetComponent<Image> ().sprite = transparentSprite;
		}
		compCard1 [0].GetComponent<Text> ().text = "";
		compCard1 [1].GetComponent<Text> ().text = "";
		compCard1 [2].GetComponent<Text> ().text = "";
		compCard1 [3].GetComponent<Text> ().text = "";
		compCard1 [4].GetComponent<Text> ().text = "";
		compCard1 [5].GetComponent<Text> ().text = "";
		compCard1 [6].GetComponent<Text> ().text = "";
		compCard1 [7].GetComponent<Text> ().text = "";
		compCard2 [0].GetComponent<Text> ().text = "";
		compCard2 [1].GetComponent<Text> ().text = "";
		compCard2 [2].GetComponent<Text> ().text = "";
		compCard2 [3].GetComponent<Text> ().text = "";
		compCard2 [4].GetComponent<Text> ().text = "";
		compCard2 [5].GetComponent<Text> ().text = "";
		compCard2 [6].GetComponent<Text> ().text = "";
		compCard2 [7].GetComponent<Text> ().text = "";
		compCard3 [0].GetComponent<Text> ().text = "";
		compCard3 [1].GetComponent<Text> ().text = "";
		compCard3 [2].GetComponent<Text> ().text = "";
		compCard3 [3].GetComponent<Text> ().text = "";
		compCard3 [4].GetComponent<Text> ().text = "";
		compCard3 [5].GetComponent<Text> ().text = "";
		compCard3 [6].GetComponent<Text> ().text = "";
		compCard3 [7].GetComponent<Text> ().text = "";
		compCard4 [0].GetComponent<Text> ().text = "";
		compCard4 [1].GetComponent<Text> ().text = "";
		compCard4 [2].GetComponent<Text> ().text = "";
		compCard4 [3].GetComponent<Text> ().text = "";
		compCard4 [4].GetComponent<Text> ().text = "";
		compCard4 [5].GetComponent<Text> ().text = "";
		compCard4 [6].GetComponent<Text> ().text = "";
		compCard4 [7].GetComponent<Text> ().text = "";
		compCard5 [0].GetComponent<Text> ().text = "";
		compCard5 [1].GetComponent<Text> ().text = "";
		compCard5 [2].GetComponent<Text> ().text = "";
		compCard5 [3].GetComponent<Text> ().text = "";
		compCard5 [4].GetComponent<Text> ().text = "";
		compCard5 [5].GetComponent<Text> ().text = "";
		compCard5 [6].GetComponent<Text> ().text = "";
		compCard5 [7].GetComponent<Text> ().text = "";
	}

	
	void OnMouseEnter() {
		Debug.Log (selectedPack);
		if (GameParameter.selectedPack == false) {
			this.GetComponent<SpriteRenderer> ().color = Color.green;
			if (this.name == "Pack1") {
				packname.text = "Garantie Pack";
				text.text = "Zieht 5 Karten, eine dieser Karten hat mit Sicherheit mindestens 7 Energy oder mehr, und fügt sie zu euren Karten hinzü.";
				coins.text = "1500";
				anzahl.text = "Anzahl der Karten: 5";
				coinsImage.SetActive (true);
			} else {
				packname.text = "Standart Pack";
				text.text = "Zieht 5 Karten und fügt sie zu euren Karten hinzü.";
				coins.text = "1000";
				anzahl.text = "Anzahl der Karten: 5";
				coinsImage.SetActive (true);
			}
		} else {
			packname.text = "Pack kaufen?";
		}
	}

	void OnMouseExit() {
		if (GameParameter.selectedPack == false) {
			packname.text = "Wähle ein Pack aus";
		} else {
			packname.text = "Pack kaufen?";
		}
		this.GetComponent<SpriteRenderer> ().color = initialColor;
		coinsImage.SetActive(false);
		text.text = "";
		coins.text = "";
		anzahl.text = "";
	}


}