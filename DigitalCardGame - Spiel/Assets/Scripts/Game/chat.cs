﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class chat : MonoBehaviour {

	public static List<string> chatHistory = new List<string>();

	private static string curr = string.Empty;
	 
	private void OnGUI() {
		if (GameParameter.bothFinishedCardSelection) {
			GUILayout.Space (2);
			GUILayout.BeginHorizontal (GUILayout.Width (250));
			curr = GUILayout.TextField (curr);
			if (GUILayout.Button ("Send")) {
				Debug.Log ("Clicked");
				networkView.RPC ("ChatMessage", RPCMode.AllBuffered, new object[] { curr });
				curr = string.Empty;
			}
			GUILayout.EndHorizontal ();

			foreach (string c in chatHistory) {
				GUILayout.Label (c);
			}
		}
	}

	[RPC]
	public void ChatMessage(string x) {
		Debug.Log ("Send");
		chatHistory.Add (x);
	}
}
