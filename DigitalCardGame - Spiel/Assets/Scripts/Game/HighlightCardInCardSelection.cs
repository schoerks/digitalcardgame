﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HighlightCardInCardSelection : MonoBehaviour {
	
	private Vector3 originalScale;
	public Color initialColor;
	public GameObject card;
	Component[] compCard;

	public Sprite basicSpriteDeleted;	
	public Sprite specialSprite1;
	public Sprite specialSprite1Deleted;
	public Sprite specialSprite2;
	public Sprite specialSprite2Deleted;
	public Sprite specialSprite3;
	public Sprite specialSprite3Deleted;
	public Sprite specialSprite4;
	public Sprite specialSprite4Deleted;

	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite1Deleted;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite2Deleted;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite3Deleted;
	public Sprite spellSpecialSprite4;
	public Sprite spellSpecialSprite4Deleted;

	public Sprite basicSprite;

	// Use this for initialization
	void Start () {
		
	}

	void OnMouseEnter () {
		originalScale = transform.localScale;
		transform.localScale = new Vector3 (1.18f, 1.18f, 1.18f);
		initialColor = card.GetComponent<Image> ().color;
		card.GetComponent<Image> ().color = Color.green;
	}
	
	void OnMouseExit () {
		card.GetComponent<Image> ().color = initialColor;
		transform.localScale = originalScale;
	}
	
	void OnMouseDown () {
		compCard = card.GetComponentsInChildren<Text> ();
		Card selectedCard = gameObject.AddComponent<Card> ();
		selectedCard.Id = System.Int32.Parse (compCard [4].GetComponent<Text> ().text);
		foreach (var playableCard in GameParameter.playableCards) {
			if (playableCard.Id == selectedCard.Id) {
				if (!playableCard.Selected) {
					playableCard.Selected = true;
					FillSpriteLikeCharacter (true, playableCard, playableCard.Text);
				} else {
					playableCard.Selected = false;
					FillSpriteLikeCharacter (false, playableCard, playableCard.Text);
				}
			}
		}
	}

	public void FillSpriteLikeCharacter (bool check, Card playableCard, string text) {
		Image image;
		if (check) {
			image = card.GetComponent<Image> ();
			if (playableCard.Character == 0) {
				if (playableCard.IsSpell) {
					image.sprite = GameParameter.spellBasicSpriteDeleted;
				} else {
					image.sprite = basicSpriteDeleted;
				}
			} else if (playableCard.Character == 1) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite1Deleted;
				} else {
					image.sprite = specialSprite1Deleted;
				}
			} else if (playableCard.Character == 2) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite2Deleted;
				} else {
					image.sprite = specialSprite2Deleted;
				}
			} else if (playableCard.Character == 3) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite3Deleted;
				} else {
					image.sprite = specialSprite3Deleted;
				}
			} else if (playableCard.Character == 4) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite4Deleted;
				} else {
					image.sprite = specialSprite4Deleted;
				}
			}
		} else if (!check) {
			image = card.GetComponent<Image> ();
			if (playableCard.Character == 0) {
				if (playableCard.IsSpell) {
					image.sprite = GameParameter.spellBasicSprite;
				} else {
					image.sprite = basicSprite;
				}
			} else if (playableCard.Character == 1) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite1;
				} else {
					image.sprite = specialSprite1;
				}
			} else if (playableCard.Character == 2) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite2;
				} else {
					image.sprite = specialSprite2;
				}
			} else if (playableCard.Character == 3) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite3;
				} else {
					image.sprite = specialSprite3;
				}
			} else if (playableCard.Character == 4) {
				if (playableCard.IsSpell) {
					image.sprite = spellSpecialSprite4;
				} else {
					image.sprite = specialSprite4;
				}
			}
		}
	}
}