﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameParameter : MonoBehaviour {

	public static List<Card> playableCards;

	public static GameObject[] playableCardsGameObjects;
	public static GameObject[] opponentCardsGameObjects;

	public static GameObject Diener;
	public static GameObject Distraction;
	public static List<GameObject> allServants;
	public static List<GameObject> allServantsOpponent;

	public static int energyCounter;

	public static NetworkPlayer player1;
	public static NetworkPlayer player2;

	public static GameObject myServant = null;
	public static GameObject opponentObject = null;
	public static GameObject specialAttack = null;

	public static GameObject opponent;
	public static GameObject character;

	public static GameObject characterSpecialAttack;
	public static GameObject opponentSpecialAttack;

	public static bool myTurn;

	public static bool bothFinishedCardSelection;

	public static int roundCounter;

	public static float timer;

	public static bool gameFinished;

	public static int characterID;

	public static Texture[] playedServantsDesign;

	public static bool selectedPack;

	public static Sprite basicSprite;
	public static Sprite specialSprite1;
	public static Sprite specialSprite2;
	public static Sprite specialSprite3;
	public static Sprite specialSprite4;

	public static Sprite spellBasicSprite;
	public static Sprite spellBasicSpriteAdded;
	public static Sprite spellBasicSpriteDeleted;
	public static Sprite spellSpecialSprite1;
	public static Sprite spellSpecialSprite2;
	public static Sprite spellSpecialSprite3;
	public static Sprite spellSpecialSprite4;

	public static Texture myTurnTexture;
	public static Texture notMyTurnTexture;

	public static GameObject cardToDrawAndAnimate1;
	public static GameObject cardToDrawAndAnimate2;
	public static GameObject cardToDrawAndAnimate3;
	public static GameObject cardToDrawAndAnimate4;

	public static GameObject cardToDrawAndAnimateOpp1;
	public static GameObject cardToDrawAndAnimateOpp2;
	public static GameObject cardToDrawAndAnimateOpp3;
	public static GameObject cardToDrawAndAnimateOpp4;

	public static int howManyCardsToDrawForOpponent;

	public static bool lockBool;
}
