﻿using UnityEngine;
using System.Collections;

public class AnimateMyGates : MonoBehaviour {

	public GameObject leftGate;
	public GameObject rightGate;

	bool closed;
	
	// Use this for initialization
	void Start () {
		closed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hitInfo = new RaycastHit ();
			bool hit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
			if (hit && hitInfo.transform.gameObject.name == "MyRigthGate"
			    || hitInfo.transform.gameObject.name == "MyLeftGate") {
				if (!closed) {
					Debug.Log (hit.ToString ());
					rightGate.GetComponent<Animator>().Play("CloseMySecondGate");
					leftGate.GetComponent<Animator>().Play("CloseMyGate");
					closed = true;
				} else {
					rightGate.GetComponent<Animator>().Play("OpenMySecondGate");
					leftGate.GetComponent<Animator>().Play("OpenMyGate");
					closed = false;
				}
			}
		}
	}
}
