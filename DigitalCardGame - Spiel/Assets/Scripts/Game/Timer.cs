﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class Timer : MonoBehaviour {

	public Text timerOnScreen;
	public Image imageTimer;
	float time = 50f;

	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;

	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;

	public GameObject attackDistractionBeforeAnythingOtherPanel;
	public GameObject attackDistractionBeforeAnythingOtherBackground;
	public Text attackDistractionBeforeAnythingOtherText;

	public GameObject readyButton;

	// Use this for initialization
	void Start () {
		GameParameter.timer = time;
		GameParameter.gameFinished = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (/*GameParameter.bothFinishedCardSelection && */!GameParameter.gameFinished) {
			if (GameParameter.timer <= 0) {
				if (GameParameter.myTurn) {
					GameParameter.myTurn = false;
					readyButton.renderer.materials[0].mainTexture = GameParameter.notMyTurnTexture;
					GameParameter.timer = time;
					if (GameParameter.roundCounter < 10) {
						GameParameter.roundCounter++;
					}
					GameParameter.energyCounter = GameParameter.roundCounter;
					timerOnScreen.color = Color.black;
					networkView.RPC ("NotifyOpponentThatItIsHisTurn", RPCMode.Others, null);
					setNameOfServants();
					ClickToAttack.colorDiener();
					GameObject psObject;
					foreach (var item in GameParameter.allServantsOpponent) {
						item.transform.Find("Main").renderer.materials[1].color = Color.white;
						item.GetComponent<Servant>().isFirstRound = false;
						psObject = item.transform.Find("Zzz").gameObject;
						psObject.particleSystem.Stop();
						psObject.SetActive(false);
					}
				}
			}
			if (GameParameter.timer <= 10) {
				timerOnScreen.color = Color.red;
			} else {
				timerOnScreen.color = Color.black;
			}
			GameParameter.timer -= Time.deltaTime;
			timerOnScreen.text = "" + GameParameter.timer.ToString ("0");
			imageTimer.fillAmount = GameParameter.timer / 50;
		}
	}

	public static void setNameOfServants() {
		foreach (var servant in GameParameter.allServants) {
			String[] name = servant.name.Split (';');
			servant.name = name [0] + ";" + name [1] + ";" + name [2] + ";0";
		}
		GameObject psObject;
		foreach (var item in GameParameter.allServantsOpponent) {
			item.GetComponent<Servant>().isFirstRound = false;
			psObject = item.transform.Find("Zzz").gameObject;
			psObject.particleSystem.Stop();
			psObject.SetActive(false);
		}
		GameParameter.specialAttack.name = "SpecialAttack;0";
		ClickToAttack.colorDiener ();
	}

	[RPC]
	public void NotifyOpponentThatItIsHisTurn() {
		Debug.Log ("I have to wait");
		timerOnScreen.color = Color.black;
		GameParameter.timer = time;
		GameParameter.myTurn = true;
		readyButton.renderer.materials[0].mainTexture = GameParameter.myTurnTexture;
		Debug.Log ("Actual round: " + GameParameter.roundCounter + " Energie: " + GameParameter.energyCounter);
		if (GameParameter.playableCards.Count() < 8) {
			int x = UnityEngine.Random.Range (0, Parameter.deckCards.Count);
			GameParameter.playableCards.Add (Parameter.deckCards [x]);
			Parameter.deckCards.RemoveAt (x);
			HandelUserdata.FillPlayableCardsUserData();
		}
		StartCoroutine (activatePanel());
	}
	
	public IEnumerator  activatePanel(){
		attackDistractionBeforeAnythingOtherText.text = "Du bist am Zug!";
		attackDistractionBeforeAnythingOtherPanel.SetActive (true);
		attackDistractionBeforeAnythingOtherPanel.GetComponent<Animator>().Play("NotificationSlideIn");
		yield return new WaitForSeconds(3.2f);
		attackDistractionBeforeAnythingOtherPanel.SetActive (false);
	}

	/*public void FillPlayableCardsTimer() {
		for (int i = 0; i < GameParameter.playableCards.Count(); i++) {
			GameParameter.playableCardsGameObjects[i].SetActive(true);
		}
		int count = 0;
		foreach (var card in Parameter.playableCards) {
			Debug.Log (card.Id + " " + card.Angriff + " " + card.Leben + " " + card.Energy);
		}
		foreach (var card in GameParameter.playableCards) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			string[] splitText = card.Text.Split(' ');
			if (card.IsSpell) {
				compCard [0].GetComponent<Text> ().text = "";
				compCard [1].GetComponent<Text> ().text = "";
			} else {
				compCard [0].GetComponent<Text> ().text = card.Leben.ToString();
				compCard [1].GetComponent<Text> ().text = card.Angriff.ToString();
			}
			compCard[2].GetComponent<Text>().text = card.Energy.ToString();
			compCard[3].GetComponent<Text>().text = card.Character.ToString();
			compCard[4].GetComponent<Text>().text = card.Id.ToString ();
			compCard[5].GetComponent<Text>().text = card.Text;
			if (card.Character == 0) {
				if (card.IsSpell) {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellBasicSprite;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.basicSprite;
				}
			} else if (card.Character == 1) {
				if (card.IsSpell) {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite1;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite1;
				}
			} else if (card.Character == 2) {
				if (card.IsSpell) {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite2;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite2;
				}
			} else if (card.Character == 3) {
				if (card.IsSpell) {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite3;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite3;
				}
			} else if (card.Character == 4) {
				if (card.IsSpell) {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite4;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite4;
				}
			}
			/*if (card.Character == 0) {
				cardObject.GetComponent<SpriteRenderer>().sprite = basicSprite;
			} else if (card.Character == 1) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite1;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite1;
				}
			} else if (card.Character == 2) {
				if (splitText[0] == "Fügt") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite2;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite2;
				}
			} else if (card.Character == 3) {
				if (splitText[0] == "Zieht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite3;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite3;
				}
			} else if (card.Character == 4) {
				if (splitText[0] == "Erhöht") {
					cardObject.GetComponent<SpriteRenderer>().sprite = spellSpecialSprite4;
				} else {
					cardObject.GetComponent<SpriteRenderer>().sprite = specialSprite4;
				}
			}*/
			/*cardObject.GetComponent<BoxCollider>().enabled = true;
			count++;
		}
		while (count < GameParameter.playableCardsGameObjects.Count()) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			//GameParameter.playableCardsGameObjects[count].SetActive(false);
			compCard[0].GetComponent<Text>().text = "";
			compCard[1].GetComponent<Text>().text = "";
			compCard[2].GetComponent<Text>().text = "";
			compCard[3].GetComponent<Text>().text = "";
			compCard[4].GetComponent<Text>().text = "";
			compCard[5].GetComponent<Text>().text = "";
			cardObject.GetComponent<SpriteRenderer>().sprite = null;
			cardObject.GetComponent<BoxCollider>().enabled = false;
			count++;
		}
		foreach (var c in GameParameter.playableCardsGameObjects) {
			string[] name = c.name.Split(';');
			c.name = name[0]+";0";
		}
	}*/
}
