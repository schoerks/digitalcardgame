using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Linq;
public class GiveCardsForFirstRound : MonoBehaviour {
	List<Card> cards;
	List<Card> c;
	public GameObject Diener;
	public GameObject card1;
	public GameObject card2;
	public GameObject card3;
	public GameObject card4;
	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;
	public Sprite spellBasicSprite;
	public Sprite spellBasicSpriteAdded;
	public Sprite spellBasicSpriteDeleted;
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;
	public Texture specialAttackSprite1; // Which are actually
	public Texture specialAttackSprite2; // Textures
	public Texture specialAttackSprite3;
	public Texture specialAttackSprite4;
	Component[] compCard1 = null;
	Component[] compCard2 = null;
	Component[] compCard3 = null;
	Component[] compCard4 = null;
	GameObject[] cardObjects;
	public GameObject canvas;
	public GameObject cardsObject;
	public GameObject opponentCards;
	public GameObject character;
	public GameObject characterSpecialAttack;
	public GameObject opponent;
	//public GameObject opponentSpecialAttack;
	public GameObject card1O;
	public GameObject card2O;
	public GameObject card3O;
	public GameObject card4O;
	public GameObject card5O;
	public GameObject card6O;
	public GameObject card7O;
	public GameObject card8O;
	public GameObject[] opponentCardsObjects;
	static GameObject[] cardsGameObjects;
	public Texture characterShader1;
	public Texture characterShader2;
	public Texture characterShader3;
	public Texture characterShader4;
	public int characterId;
	public GameObject UserdataPanel;
	public GameObject WaitForSecondPlayerPanel;
	public bool amIReady;
	public bool isHeReady;
	List<Card> cardsToSelect;

	public GameObject readyButton;
	public GameObject readyButtonParent; // For animation
	public Texture myTurnTexture;
	public Texture notMyTurnTexture;

	int checkTimer = 0;

	// Use this for initialization
	void Start () {
		GameParameter.timer = 30f;
		GameParameter.spellBasicSprite = spellBasicSprite;
		GameParameter.spellBasicSpriteAdded = spellBasicSpriteAdded;
		GameParameter.spellBasicSpriteDeleted = spellBasicSpriteDeleted;
		GameParameter.myTurnTexture = myTurnTexture;
		GameParameter.notMyTurnTexture = notMyTurnTexture;
		GameParameter.opponentCardsGameObjects = opponentCardsObjects;
		GameParameter.opponent = opponent;
		GameParameter.character = character;
		GameParameter.characterSpecialAttack = characterSpecialAttack;
		cardsToSelect = new List<Card>(Parameter.deckCards);
		GameParameter.roundCounter = 1;
		amIReady = false;
		isHeReady = false;
		GameParameter.myTurn = false;
		WaitForSecondPlayerPanel.SetActive (false);
		//int counter = 0;
		characterId = GameParameter.characterID;
		if (characterId == 1) {
			character.renderer.materials[1].mainTexture = characterShader1;
			characterSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite1;
		} else if (characterId == 2) {
			character.renderer.materials[1].mainTexture = characterShader2;
			characterSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite2;
		} else if (characterId == 3) {
			character.renderer.materials[1].mainTexture = characterShader3;
			characterSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite3;
		} else if (characterId == 4) {
			character.renderer.materials[1].mainTexture = characterShader4;
			characterSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite4;
		}
		GameParameter.characterID = characterId;
		networkView.RPC("SetChar",RPCMode.Others, new object[] {characterId});
		UserdataPanel.SetActive (false);
		GameParameter.Diener = Diener;
		GameParameter.allServants = new List<GameObject> ();
		GameParameter.allServantsOpponent = new List<GameObject> ();
		GameObject[] a = new GameObject[8];
		a [0] = card1O;
		a [1] = card2O;
		a [2] = card3O;
		a [3] = card4O;
		a [4] = card5O;
		a [5] = card6O;
		a [6] = card7O;
		a [7] = card8O;
		cardsGameObjects = a;
		GameParameter.playableCardsGameObjects = cardsGameObjects;
		cardsObject.SetActive(false);
		opponentCards.SetActive (false);
		canvas.gameObject.SetActive (true);
		character.SetActive (false);
		opponent.SetActive (false);
		/**//*List<Card> l = new List<Card> ();
		for (int i = 0; i < 30; i++) {
			Card card = gameObject.AddComponent<Card> ();
			card.Id = i;
			card.Angriff = i;
			card.Energy = 2;
			card.Leben = i;
			card.Text = "Erhöht adf adfa";
			card.Selected = false;
			card.Character = 1;
			l.Add(card);
		}
		Parameter.deckCards = l;*//**/
		cardObjects = new GameObject[] {card1, card2, card3, card4};
		compCard1 = cardObjects[0].GetComponentsInChildren<Text> ();
		compCard2 = cardObjects[1].GetComponentsInChildren<Text> ();
		compCard3 = cardObjects[2].GetComponentsInChildren<Text> ();
		compCard4 = cardObjects[3].GetComponentsInChildren<Text> ();
		BoxCollider bc;
		foreach (var cardObject in cardObjects) {
			bc = cardObject.AddComponent<BoxCollider>();
			bc.size = new Vector3(142.8f, 204.8f,40);
		}
		cards = new List<Card>();
		System.Random random = new System.Random ();
		int[] randomNumbers = Enumerable.Range(0,cardsToSelect.Count-1).OrderBy(x => random.Next()).Take(4).ToArray();
		Card[] cardArray = new Card[4];
		cardArray [0] = cardsToSelect [randomNumbers [0]];
		cardArray [1] = cardsToSelect [randomNumbers [1]];
		cardArray [2] = cardsToSelect [randomNumbers [2]];
		cardArray [3] = cardsToSelect [randomNumbers [3]];
		foreach (var c in cardArray) {
			Debug.Log("Id: " + c.Id + " text: " + c.Text);
		}
		cards.Add (cardArray [0]);
		cards.Add (cardArray [1]);
		cards.Add (cardArray [2]);
		cards.Add (cardArray [3]);
		cardsToSelect.Remove(cardArray[0]);
		cardsToSelect.Remove(cardArray[1]);
		cardsToSelect.Remove(cardArray[2]);
		cardsToSelect.Remove(cardArray[3]);
		FillCards (cards);
		GameParameter.playableCards = cards;
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.LoadLevel ("MainMenu");
		}
		if (isHeReady && amIReady) {
			WaitForSecondPlayerPanel.SetActive (false);
			GameParameter.bothFinishedCardSelection = true;
			//networkView.RPC ("ActiveCardsOpponent", RPCMode.Others, GameParameter.playableCards.Count);
		}
		if (!amIReady) {
			Debug.Log (GameParameter.timer);
			if (GameParameter.timer <= 0) {
				checkTimer++;
				if (checkTimer == 1) {
					ClickedReady();
				}
			}
		}	
	}

	/*[RPC]
	public void ActiveCardsOpponent(int anzahl) {
		for (int i = 0; i < anzahl; i++) {
			GameParameter.opponentCardsGameObjects[i].SetActive(true);
		}
		for (int i = 7; i >= anzahl; i--) {
			GameParameter.opponentCardsGameObjects[i].SetActive(false);
		}
	}*/


	//NIcht Löschen
	/*public IEnumerator AnimateIntro () {
	introCharacter1.GetComponent<Animator> ().Play ("Character1SlideIn");
	introCharacter2.GetComponent<Animator> ().Play ("Character2SlideIn");
	introText.GetComponent<Animator> ().Play ("VisibleText");
	yield return new WaitForSeconds (3f);
	introPanel.SetActive (false);
	canvas.SetActive (true);
	}
	public void SelectCharacters() {
	int counter = 0;
	characterId = 0;
	while (characterId == 0) {
	if (Parameter.deckCards [counter].Character != 0) {
	characterId = Parameter.deckCards [counter].Character;
	}
	counter++;
	}
	if (characterId == 1) {
	introCharacter1.renderer.materials[1].mainTexture = characterShader1;
	character.renderer.materials [1].mainTexture = characterShader1;
	} else if (characterId == 2) {
	introCharacter1.renderer.materials[1].mainTexture = characterShader2;
	character.renderer.materials [1].mainTexture = characterShader2;
	} else if (characterId == 3) {
	introCharacter1.renderer.materials[1].mainTexture = characterShader3;
	character.renderer.materials [1].mainTexture = characterShader3;
	} else if (characterId == 4) {
	introCharacter1.renderer.materials[1].mainTexture = characterShader4;
	character.renderer.materials [1].mainTexture = characterShader4;
	}
	GameParameter.characterID = characterId;
	networkView.RPC ("SetChar", RPCMode.Others, new object[] {characterId});
	}*/
	public void FillCards (List<Card> cards) 
	{ 
		Image image;
		if (cards[0].IsSpell) {
			compCard1 [0].GetComponent<Text> ().text = "";
			compCard1 [1].GetComponent<Text> ().text = "";
		} else {
			compCard1 [0].GetComponent<Text> ().text = cards [0].Leben.ToString ();
			compCard1 [1].GetComponent<Text> ().text = cards [0].Angriff.ToString ();
		}
		compCard1 [2].GetComponent<Text> ().text = cards [0].Energy.ToString ();
		compCard1 [3].GetComponent<Text> ().text = cards [0].Text.ToString ();
		compCard1 [4].GetComponent<Text> ().text = cards [0].Id.ToString ();
		compCard1 [5].GetComponent<Text> ().text = cards [0].Character.ToString ();
		if (cards[1].IsSpell) {
			compCard2 [0].GetComponent<Text> ().text = "";
			compCard2 [1].GetComponent<Text> ().text = "";
		} else {
			compCard2 [0].GetComponent<Text> ().text = cards [1].Leben.ToString ();
			compCard2 [1].GetComponent<Text> ().text = cards [1].Angriff.ToString ();
		}
		compCard2 [2].GetComponent<Text> ().text = cards [1].Energy.ToString ();
		compCard2 [3].GetComponent<Text> ().text = cards [1].Text.ToString ();
		compCard2 [4].GetComponent<Text> ().text = cards [1].Id.ToString ();
		compCard2 [5].GetComponent<Text> ().text = cards [1].Character.ToString ();
		if (cards[2].IsSpell) {
			compCard3 [0].GetComponent<Text> ().text = "";
			compCard3 [1].GetComponent<Text> ().text = "";
		} else {
			compCard3 [0].GetComponent<Text> ().text = cards [2].Leben.ToString ();
			compCard3 [1].GetComponent<Text> ().text = cards [2].Angriff.ToString ();
		}
		compCard3 [2].GetComponent<Text> ().text = cards [2].Energy.ToString ();
		compCard3 [3].GetComponent<Text> ().text = cards [2].Text.ToString ();
		compCard3 [4].GetComponent<Text> ().text = cards [2].Id.ToString ();
		compCard3 [5].GetComponent<Text> ().text = cards [2].Character.ToString ();
		if (cards[3].IsSpell) {
			compCard4 [0].GetComponent<Text> ().text = "";
			compCard4 [1].GetComponent<Text> ().text = "";
		} else {
			compCard4 [0].GetComponent<Text> ().text = cards [3].Leben.ToString ();
			compCard4 [1].GetComponent<Text> ().text = cards [3].Angriff.ToString ();
		}
		compCard4 [2].GetComponent<Text> ().text = cards [3].Energy.ToString ();
		compCard4 [3].GetComponent<Text> ().text = cards [3].Text.ToString ();
		compCard4 [4].GetComponent<Text> ().text = cards [3].Id.ToString ();
		compCard4 [5].GetComponent<Text> ().text = cards [3].Character.ToString ();
		int i = 0;
		foreach (var cardObject in cardObjects) {
			image = cardObject.GetComponent<Image> ();
			ChooseSpecialCardFromCharacter (image, cards[i].Character, cards[i].IsSpell);
			i++;
		}
	}
	public void ChooseSpecialCardFromCharacter (Image image, int characterId, bool isSpell) {
		if (characterId == 0) {
			if (isSpell) {
				image.sprite = spellBasicSprite;
			} else {
				image.sprite = basicSprite;
			}
		} else if (characterId == 1) {
			if (isSpell) {
				image.sprite = spellSpecialSprite1;
			} else {
				image.sprite = specialSprite1;
			}
		} else if (characterId == 2) {
			if (isSpell) {
				image.sprite = spellSpecialSprite2;
			} else {
				image.sprite = specialSprite2;
			}
		} else if (characterId == 3) {
			if (isSpell) {
				image.sprite = spellSpecialSprite3;
			} else {
				image.sprite = specialSprite3;
			}
		} else if (characterId == 4) {
			if (isSpell) {
				image.sprite = spellSpecialSprite4;
			} else {
				image.sprite = specialSprite4;
			}
		}
	}
	public void ClickedReady() {
		StartCoroutine (Animate ());
		List<Card> l = new List<Card> ();
		foreach (var card in cards) {
			if (!card.Selected) {
				l.Add(card);
				Parameter.deckCards.Remove(card);
			} else {
				int x = UnityEngine.Random.Range(0, cardsToSelect.Count);
				Card c = cardsToSelect[x];
				cardsToSelect.Remove(c);
				Parameter.deckCards.Remove(c);
				l.Add(c);
			}
		}
		GameParameter.playableCards = l;
		character.SetActive (true);
		opponent.SetActive (true);
		opponentCards.SetActive (true);
		cardsObject.SetActive (true);
		FillFirstCards ();
		amIReady = true;
		if (!isHeReady) {
			GameParameter.myTurn = true;
			WaitForSecondPlayerPanel.SetActive (true);
			readyButton.renderer.materials[0].mainTexture = GameParameter.myTurnTexture;
			networkView.RPC ("NotifyOpponentThatIAmReadyAndItsMyTurn", RPCMode.Others, null);
		} else {
			GameParameter.timer = 50f;
			networkView.RPC ("NotifyOpponentThatIAmReady", RPCMode.Others, null);
		}
	}
	public IEnumerator Animate() {
		UserdataPanel.SetActive (true);
		UserdataPanel.GetComponent<Animator>().Play("UserDataSlideIn");
		canvas.GetComponent<Animator>().Play("FirstCardSelectionSlideOut"); 
		readyButtonParent.GetComponent<Animator> ().Play ("ReadyButtonSlideIn");
		yield return new WaitForSeconds(1.6f);
		canvas.SetActive (false);
	}
	[RPC]
	public void NotifyOpponentThatIAmReady() {
		isHeReady = true;
		GameParameter.timer = 50f;
	}
	[RPC]
	public void NotifyOpponentThatIAmReadyAndItsMyTurn() {
		isHeReady = true;
		GameParameter.myTurn = false;
		readyButton.renderer.materials[0].mainTexture = GameParameter.notMyTurnTexture;
	}
	void FillFirstCards() {
		Card[] l = GameParameter.playableCards.ToArray();
		GameObject[] cards = cardsGameObjects;
		for (int i = 0; i < 4; i++) {
			Component[] compCard = cards[i].GetComponentsInChildren<Text>();
			if (l[i].IsSpell) {
				compCard [0].GetComponent<Text> ().text = "";
				compCard [1].GetComponent<Text> ().text = "";
			} else {
				compCard [0].GetComponent<Text> ().text = l[i].Leben.ToString();
				compCard [1].GetComponent<Text> ().text = l[i].Angriff.ToString();
			}
			compCard[2].GetComponent<Text>().text = l[i].Energy.ToString();
			compCard[3].GetComponent<Text>().text = l[i].Character.ToString();
			compCard[4].GetComponent<Text>().text = l[i].Id.ToString ();
			compCard[5].GetComponent<Text>().text = l[i].Text;
			if (l[i].Character == 0) {
				if (l[i].IsSpell) {
					cards[i].GetComponent<SpriteRenderer>().sprite = spellBasicSprite;
				} else {
					cards[i].GetComponent<SpriteRenderer>().sprite = basicSprite;
				}
			} else if (l[i].Character == 1) {
				if (l[i].IsSpell) {
					cards[i].GetComponent<SpriteRenderer>().sprite = spellSpecialSprite1;
				} else {
					cards[i].GetComponent<SpriteRenderer>().sprite = specialSprite1;
				}
			} else if (l[i].Character == 2) {
				if (l[i].IsSpell) {
					cards[i].GetComponent<SpriteRenderer>().sprite = spellSpecialSprite2;
				} else {
					cards[i].GetComponent<SpriteRenderer>().sprite = specialSprite2;
				}
			} else if (l[i].Character == 3) {
				if (l[i].IsSpell) {
					cards[i].GetComponent<SpriteRenderer>().sprite = spellSpecialSprite3;
				} else {
					cards[i].GetComponent<SpriteRenderer>().sprite = specialSprite3;
				}
			} else if (l[i].Character == 4) {
				if (l[i].IsSpell) {
					cards[i].GetComponent<SpriteRenderer>().sprite = spellSpecialSprite4;
				} else {
					cards[i].GetComponent<SpriteRenderer>().sprite = specialSprite4;
				}
			}
		}
		for (int i = 4; i < cards.Length; i++) {
			//cards[i].SetActive(false);
			GameObject cardObject = GameParameter.playableCardsGameObjects[i];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			//GameParameter.playableCardsGameObjects[count].SetActive(false);
			compCard[0].GetComponent<Text>().text = "";
			compCard[1].GetComponent<Text>().text = "";
			compCard[2].GetComponent<Text>().text = "";
			compCard[3].GetComponent<Text>().text = "";
			compCard[4].GetComponent<Text>().text = "";
			compCard[5].GetComponent<Text>().text = "";
			cardObject.GetComponent<SpriteRenderer>().sprite = null;
			cardObject.GetComponent<BoxCollider>().enabled = false;
		}
	}
	[RPC]
	public void SetChar (int characterId){
		if (characterId == 1) {
			opponent.renderer.materials[1].mainTexture = characterShader1;
			//opponentSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite1;
		} else if (characterId == 2) {
			opponent.renderer.materials[1].mainTexture = characterShader2;
			//opponentSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite2;
		} else if (characterId == 3) {
			opponent.renderer.materials[1].mainTexture = characterShader3;
			//opponentSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite3;
		} else if (characterId == 4) {
			opponent.renderer.materials[1].mainTexture = characterShader4;
			//opponentSpecialAttack.renderer.materials[1].mainTexture = specialAttackSprite4;
		}
	}
}