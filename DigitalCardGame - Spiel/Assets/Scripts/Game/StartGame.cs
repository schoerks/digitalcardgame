﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class StartGame : MonoBehaviour {
	HostData[] hostData;
	public GameObject panel;
	public Text panelText;
	public GameObject panelButtonPanel;
	public Text panelButtonText;
	public GameObject Game;
	public GameObject Search;
	public GameObject startServer;
	public GameObject loadAnimation;
	public Image loadImage;
	bool check;
	public static int coins;
	public static int points;
	public static int won;
	public static int lost;
	void Awake ()
	{
	}
	void Update () {
		if (check) {
			loadImage.transform.Rotate(0.0f, 0.0f, -1.0f);
		}
	}
	void Start () {
		coins = Parameter.Coins;
		points = Parameter.Levelpoints;
		won = Parameter.GamesWon;
		lost = Parameter.GamesLost;
		check = false;
		panel.SetActive (false);
		//Game.SetActive (false);
		hostData = null;
	}
	public void MakeConnection () {
		hostData = null;
		check = true;
		panel.SetActive (true);
		MasterServer.RequestHostList (Parameter.registeredGameName);
	}
	public void ClickedCancel () {
		Application.LoadLevel ("MainMenu");
	}
	public void RefreshHostList () {
		MasterServer.RequestHostList (Parameter.registeredGameName);
	}
	void OnMasterServerEvent (MasterServerEvent ms) {
		if (ms == MasterServerEvent.HostListReceived) {
			hostData = MasterServer.PollHostList();
			Debug.Log ("hostData; list received: " + hostData.Length);
			if (hostData != null || hostData.Length != 0) {
				check = false;
				for (int i = 0; i < hostData.Length; i++) {
					if (hostData[i].connectedPlayers < 2) {
						Network.Connect (hostData [i]);
						Debug.Log ("Connected to " + hostData [i].gameName + "");
						check = true;
					}
				}
				if (!check) {
					panel.SetActive(true);
					loadAnimation.SetActive(false);
					check = false;
					panelButtonPanel.SetActive (true);
					panelText.text = "Ein Gegner wird gesucht...";
					panelButtonText.text = "Abbrechen";
					Network.InitializeServer (100, 1521, true);
					MasterServer.RegisterHost(Parameter.registeredGameName, Parameter.Username+"DCG");
					Debug.Log ("Created Server " + Parameter.registeredGameName);
				}
			}
		}
	}
	public void ClickedOK() {
		panel.SetActive(false);
		panelButtonPanel.SetActive (false);
		panelText.text = "Gegner wird gesucht...";
		loadAnimation.SetActive (true);
		check = true;
	}
	[RPC]
	void AcceptClient () {
	}
	void OnConnectedToServer()
	{
		networkView.RPC ("AddPlayer", RPCMode.Server);
		Debug.Log("Server Joined");
	}
	void OnServerInitialized() 
	{
		Debug.Log("Server Initializied");
	}
	[RPC]
	public void LoadGame () {
		Game.SetActive (true);
		Search.SetActive (false);
		//StartCoroutine (ChangeScene("GamePlay"));
	}
}