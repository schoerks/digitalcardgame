﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class HandelUserdata : MonoBehaviour {
	
	public GameObject EnergyPanel;
	
	public Image e1;
	public Image e2;
	public Image e3;
	public Image e4;
	public Image e5;
	public Image e6;
	public Image e7;
	public Image e8;
	public Image e9;
	public Image e10;
	
	public Image[] energy;
	
	public Color initialColor;
	
	public Sprite basicSprite;
	public Sprite specialSprite1;
	public Sprite specialSprite2;
	public Sprite specialSprite3;
	public Sprite specialSprite4;
	
	public Sprite spellSpecialSprite1;
	public Sprite spellSpecialSprite2;
	public Sprite spellSpecialSprite3;
	public Sprite spellSpecialSprite4;
	
	public GameObject attackDistractionBeforeAnythingOtherPanel;
	public GameObject attackDistractionBeforeAnythingOtherBackground;
	public Text attackDistractionBeforeAnythingOtherText;
	
	public GameObject servant;
	public GameObject attackServantsParticleOpponent;
	public Texture attackPointsKaPoom;
	
	public GameObject readyButton;
	public Text timerOnScreen;
	
	public GameObject card;

	public Text EnergyCounter;
	public Text CardCounter;

	public GameObject cardToDrawAndAnimate1;
	public GameObject cardToDrawAndAnimate2;
	public GameObject cardToDrawAndAnimate3;
	public GameObject cardToDrawAndAnimate4;

	public GameObject cardToDrawAndAnimateOpp1;
	public GameObject cardToDrawAndAnimateOpp2;
	public GameObject cardToDrawAndAnimateOpp3;
	public GameObject cardToDrawAndAnimateOpp4;

	// Use this for initialization
	void Start () {
		GameParameter.howManyCardsToDrawForOpponent = 0;
		GameParameter.lockBool = false;
		GameParameter.cardToDrawAndAnimate1 = cardToDrawAndAnimate1;
		GameParameter.cardToDrawAndAnimate2 = cardToDrawAndAnimate2;
		GameParameter.cardToDrawAndAnimate3 = cardToDrawAndAnimate3;
		GameParameter.cardToDrawAndAnimate4 = cardToDrawAndAnimate4;
		GameParameter.cardToDrawAndAnimateOpp1 = cardToDrawAndAnimateOpp1;
		GameParameter.cardToDrawAndAnimateOpp2 = cardToDrawAndAnimateOpp2;
		GameParameter.cardToDrawAndAnimateOpp3 = cardToDrawAndAnimateOpp3;
		GameParameter.cardToDrawAndAnimateOpp4 = cardToDrawAndAnimateOpp4;
		GameParameter.basicSprite = basicSprite;
		GameParameter.specialSprite1 = specialSprite1;
		GameParameter.specialSprite2 = specialSprite2;
		GameParameter.specialSprite3 = specialSprite3;
		GameParameter.specialSprite4 = specialSprite4;
		GameParameter.spellSpecialSprite1 = spellSpecialSprite1;
		GameParameter.spellSpecialSprite2 = spellSpecialSprite2;
		GameParameter.spellSpecialSprite3 = spellSpecialSprite3;
		GameParameter.spellSpecialSprite4 = spellSpecialSprite4;
		if (GameParameter.myTurn) {
			readyButton.renderer.materials[0].mainTexture = GameParameter.myTurnTexture;
		} else {
			readyButton.renderer.materials[0].mainTexture = GameParameter.notMyTurnTexture;
		}
		initialColor = e1.color;
		energy = new Image[] {e1, e2,e3,e4,e5,e6,e7,e8,e9,e10};
		GameParameter.energyCounter = 1;
		
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < 10; i++) {
			if (i < GameParameter.energyCounter) {
				energy[i].color = initialColor;
				energy[i].transform.Rotate(0.0f, 0.0f, -0.5f);
			} else {
				energy[i].color = new Color(1f,1f,1f,0.5f);
				energy[i].transform.Rotate(0.0f, 0.0f, -0.5f);
			}
		}
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hitInfo = new RaycastHit ();
			bool hit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
			if (hit && hitInfo.transform.gameObject.name == "Ready") {
				Debug.Log("clicked");
				ClickedReady ();
			}
		}
		EnergyCounter.text = GameParameter.energyCounter + "/10";
		CardCounter.text = Parameter.deckCards.Count() + "/30";
	}
	
	public void ClickedReady() {
		if (GameParameter.myTurn) {
			GameParameter.timer = 50f;
			GameParameter.myTurn = false;
			timerOnScreen.color = Color.black;
			readyButton.renderer.materials[0].mainTexture = GameParameter.notMyTurnTexture;
			if (GameParameter.roundCounter < 10) {
				GameParameter.roundCounter++;
			}
			GameParameter.energyCounter = GameParameter.roundCounter;
			GameObject psObject;
			foreach (var item in GameParameter.allServantsOpponent) {
				item.transform.Find("Main").renderer.materials[1].color = Color.white;
				item.GetComponent<Servant>().isFirstRound = false;
				psObject = item.transform.Find("Zzz").gameObject;
				psObject.particleSystem.Stop();
				psObject.SetActive(false);
			}
			networkView.RPC ("ClickedReadyAndNotifyOpponentThatItIsHisTurn", RPCMode.Others, null);
			Timer.setNameOfServants();
		}
	}

	[RPC]
	public void ClickedReadyAndNotifyOpponentThatItIsHisTurn() {
		GameParameter.myTurn = true;
		readyButton.renderer.materials[0].mainTexture = GameParameter.myTurnTexture;
		GameParameter.timer = 50f;
		timerOnScreen.color = Color.black;
		if (GameParameter.playableCards.Count () < 8) {
			int x = UnityEngine.Random.Range (0, Parameter.deckCards.Count);
			GameParameter.playableCards.Add (Parameter.deckCards [x]);
			Parameter.deckCards.RemoveAt (x);
			FillPlayableCardsUserData ();
			networkView.RPC ("DrawCardsShowOpponent", RPCMode.Others, null);
			GameParameter.howManyCardsToDrawForOpponent = 0;
		}
		StartCoroutine (activatePanel());
		networkView.RPC ("ActiveCardsOpponent", RPCMode.Others, GameParameter.playableCards.Count);
	}
	
	[RPC]
	public void ActiveCardsOpponent(int anzahl) {
		for (int i = 0; i < anzahl; i++) {
			GameParameter.opponentCardsGameObjects[i].SetActive(true);
		}
		for (int i = 7; i >= anzahl; i--) {
			GameParameter.opponentCardsGameObjects[i].SetActive(false);
		}
	}
	
	public IEnumerator  activatePanel(){
		attackDistractionBeforeAnythingOtherText.text = "Du bist am Zug!";
		attackDistractionBeforeAnythingOtherPanel.SetActive (true);
		attackDistractionBeforeAnythingOtherPanel.GetComponent<Animator>().Play("NotificationSlideIn");
		yield return new WaitForSeconds(3.2f);
		attackDistractionBeforeAnythingOtherPanel.SetActive (false);
	}

	static void FillCards(Card card, int count) {
		GameObject cardObject = GameParameter.playableCardsGameObjects[count];
		Component[] compCard = cardObject.GetComponentsInChildren<Text>();
		if (card.IsSpell) {
			compCard [0].GetComponent<Text> ().text = "";
			compCard [1].GetComponent<Text> ().text = "";
		} else {
			compCard [0].GetComponent<Text> ().text = card.Leben.ToString();
			compCard [1].GetComponent<Text> ().text = card.Angriff.ToString();
		}
		compCard[2].GetComponent<Text>().text = card.Energy.ToString();
		compCard[3].GetComponent<Text>().text = card.Character.ToString();
		compCard[4].GetComponent<Text>().text = card.Id.ToString ();
		compCard[5].GetComponent<Text>().text = card.Text;
		if (card.Character == 0) {
			if (card.IsSpell) {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellBasicSprite;
			} else {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.basicSprite;
			}
		} else if (card.Character == 1) {
			if (card.IsSpell) {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite1;
			} else {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite1;
			}
		} else if (card.Character == 2) {
			if (card.IsSpell) {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite2;
			} else {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite2;
			}
		} else if (card.Character == 3) {
			if (card.IsSpell) {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite3;
			} else {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite3;
			}
		} else if (card.Character == 4) {
			if (card.IsSpell) {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite4;
			} else {
				cardObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite4;
			}
		}
		cardObject.GetComponent<BoxCollider>().enabled = true;
	}

	static IEnumerator AnimateCardToDraw(Card card, int count, int newCardsCounter, int animCounter) {
		Debug.Log ("animCounter: "+animCounter);
		if (animCounter == 1) {
			GameParameter.howManyCardsToDrawForOpponent++;
			FillCardToDraw(card, GameParameter.cardToDrawAndAnimate1);
			GameParameter.cardToDrawAndAnimate1.GetComponent<Animator> ().Play ("DrawNewCard", -1, 0f);
			yield return new WaitForSeconds(3f);
			FillCards (card, count);
		} else if (animCounter == 2) {
			GameParameter.howManyCardsToDrawForOpponent++;
			FillCardToDraw(card, GameParameter.cardToDrawAndAnimate2);
			yield return new WaitForSeconds(1f);
			GameParameter.cardToDrawAndAnimate2.GetComponent<Animator> ().Play ("DrawNewCard", -1, 0f);
			yield return new WaitForSeconds(3f);
			FillCards (card, count);
		} else if (animCounter == 3) {
			GameParameter.howManyCardsToDrawForOpponent++;
			FillCardToDraw(card, GameParameter.cardToDrawAndAnimate3);
			yield return new WaitForSeconds(2f);
			GameParameter.cardToDrawAndAnimate3.GetComponent<Animator> ().Play ("DrawNewCard", -1, 0f);
			yield return new WaitForSeconds(3f);
			FillCards (card, count);
		} else if (animCounter == 4) {
			GameParameter.howManyCardsToDrawForOpponent++;
			FillCardToDraw(card, GameParameter.cardToDrawAndAnimate4);
			yield return new WaitForSeconds(3f);
			GameParameter.cardToDrawAndAnimate4.GetComponent<Animator> ().Play ("DrawNewCard", -1, 0f);
			yield return new WaitForSeconds(3f);
			FillCards (card, count);
		}
	}

	[RPC]
	public void DrawCardsShowOpponent() {
		GameParameter.cardToDrawAndAnimateOpp1.GetComponent<Animator> ().Play ("DrawOppNewCard", -1, 0f);
	}

	public static void FillCardToDraw(Card card, GameObject cardToDrawObject) {
		Component[] compCard = cardToDrawObject.GetComponentsInChildren<Text>();
		if (card.IsSpell) {
			compCard [0].GetComponent<Text> ().text = "";
			compCard [1].GetComponent<Text> ().text = "";
		} else {
			compCard [0].GetComponent<Text> ().text = card.Leben.ToString();
			compCard [1].GetComponent<Text> ().text = card.Angriff.ToString();
		}
		compCard[2].GetComponent<Text>().text = card.Energy.ToString();
		compCard[3].GetComponent<Text>().text = card.Character.ToString();
		compCard[4].GetComponent<Text>().text = card.Id.ToString ();
		compCard[5].GetComponent<Text>().text = card.Text;
		if (card.Character == 0) {
			if (card.IsSpell) {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellBasicSprite;
			} else {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.basicSprite;
			}
		} else if (card.Character == 1) {
			if (card.IsSpell) {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite1;
			} else {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite1;
			}
		} else if (card.Character == 2) {
			if (card.IsSpell) {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite2;
			} else {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite2;
			}
		} else if (card.Character == 3) {
			if (card.IsSpell) {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite3;
			} else {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite3;
			}
		} else if (card.Character == 4) {
			if (card.IsSpell) {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.spellSpecialSprite4;
			} else {
				cardToDrawObject.GetComponent<SpriteRenderer>().sprite = GameParameter.specialSprite4;
			}
		}
		cardToDrawObject.GetComponent<BoxCollider>().enabled = true;
	}
	
	public static void FillPlayableCardsUserData() {
		int newCardsCounter = 0;
		for (int i = 0; i < GameParameter.playableCards.Count(); i++) {
			if (GameParameter.playableCardsGameObjects[i].GetComponent<SpriteRenderer>().sprite == null) {
				newCardsCounter++;
			}
			//GameParameter.playableCardsGameObjects[i].SetActive(true);
		}
		Debug.Log ("newCardsCounter "+newCardsCounter);
		int count = 0;
		int animCounter = 1;
		foreach (var card in GameParameter.playableCards) {
			if (GameParameter.playableCards.Count() - newCardsCounter == count)  {
				StaticCoroutine.DoCoroutine(AnimateCardToDraw(card, count, newCardsCounter, animCounter));
				animCounter++;
				newCardsCounter--;
			} else {
				FillCards(card, count);
			}
			count++;
		}
		while (count < GameParameter.playableCardsGameObjects.Count()) {
			GameObject cardObject = GameParameter.playableCardsGameObjects[count];
			Component[] compCard = cardObject.GetComponentsInChildren<Text>();
			//GameParameter.playableCardsGameObjects[count].SetActive(false);
			compCard[0].GetComponent<Text>().text = "";
			compCard[1].GetComponent<Text>().text = "";
			compCard[2].GetComponent<Text>().text = "";
			compCard[3].GetComponent<Text>().text = "";
			compCard[4].GetComponent<Text>().text = "";
			compCard[5].GetComponent<Text>().text = "";
			cardObject.GetComponent<SpriteRenderer>().sprite = null;
			cardObject.GetComponent<BoxCollider>().enabled = false;
			count++;
		}
		foreach (var c in GameParameter.playableCardsGameObjects) {
			string[] name = c.name.Split(';');
			c.name = name[0]+";0";
		}
	}
	
	public void ClickedFinish(){
		Network.Disconnect ();
		MasterServer.UnregisterHost ();
		Application.LoadLevel ("MainMenu");
	}
}