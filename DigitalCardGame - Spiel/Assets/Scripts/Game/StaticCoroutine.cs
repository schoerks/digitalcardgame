﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class StaticCoroutine : MonoBehaviour {

	static public StaticCoroutine instance; //the instance of our class that will do the work

	void Awake(){ //called when an instance awakes in the game
		
		instance = this; //set our static reference to our newly initialized instance
		
	}
	IEnumerator Perform(IEnumerator coroutine)
	{
		yield return StartCoroutine(coroutine);
	}
	
	static public void DoCoroutine(IEnumerator coroutine){
		
		instance.StartCoroutine(instance.Perform(coroutine)); //this will launch the coroutine on our instance
		
	}
}
