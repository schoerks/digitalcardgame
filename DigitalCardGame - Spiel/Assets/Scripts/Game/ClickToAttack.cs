using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;
public class ClickToAttack : MonoBehaviour {
	public Texture[] lifePoints;
	public Texture[] attackPoints;
	public Text opponentLifePoints;
	public Text myCharacterLifePoints;
	public GameObject blackbackgroundPanel;
	public GameObject particleSystemPanel;
	public GameObject finishedPanel;
	public GameObject specialAttack;
	public Text wonOrLostText;
	public Text coinsGained;
	public Text levelpointsGained;
	public GameObject cardsOpponent;
	public GameObject attackDistractionBeforeAnythingOtherPanel;
	public GameObject attackDistractionBeforeAnythingOtherBackground;
	public Text attackDistractionBeforeAnythingOtherText;
	bool gotCoins;
	public Texture[] attackPointsKaPoom;
	public Texture[] lifePointsKaPoom;
	public GameObject Diener;
	public Texture[] playedServantsDesign;
	public Vector3 originPosition;
	public Quaternion originRotation;
	public float shakeDecay;
	public float shakeIntensity;
	// Use this for initialization
	void Start () {
		GameParameter.specialAttack = specialAttack;
		gotCoins = false;
	}
	// Update is called once per frame
	void Update () {
		/*if (Input.GetKeyDown(KeyCode.P)) {
			Shake();
		}*/
		if (shakeIntensity > 0){
			transform.position = originPosition + UnityEngine.Random.insideUnitSphere * shakeIntensity;
			transform.rotation = new Quaternion(
				originRotation.x + UnityEngine.Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.y + UnityEngine.Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.z + UnityEngine.Random.Range (-shakeIntensity,shakeIntensity) * .1f,
				originRotation.w + UnityEngine.Random.Range (-shakeIntensity,shakeIntensity) * .1f);
			shakeIntensity -= shakeDecay;
		}
		if (GameParameter.myTurn) {
			if (Input.GetMouseButtonDown (0)) {
				RaycastHit hitInfo = new RaycastHit ();
				bool hit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
				if (hit) {
					string[] name = hitInfo.transform.gameObject.name.Split (';');
					bool isDistraction = checkForDistraction();
					string[] myServantName = new string[10];
					if (GameParameter.myServant != null) {
						myServantName = GameParameter.myServant.name.Split(';');
					}
					if (GameParameter.myServant != null && GameParameter.myServant.name == "Tötet") {
						if (name [0] == "DienerGegner") {
							GameParameter.opponentObject = hitInfo.transform.gameObject;
							networkView.RPC ("DeleteServant", RPCMode.Others, new object[] { 
								GameParameter.opponentObject.name,
								"100"
							});
							GameObject psObject = GameParameter.opponentObject.transform.Find("GotAttacked").gameObject;
							StartCoroutine(ParticleSystemEmit(GameParameter.opponentObject,psObject, "100"));
							GameParameter.allServantsOpponent.Remove (GameParameter.opponentObject);
							GameParameter.opponentObject.name = "tot";
							GameParameter.myServant = null;
							GameParameter.opponentObject = null;
						}
					} else if (GameParameter.myServant != null && GameParameter.myServant.name == "Ablenkung") {
						if (name [0] == "Diener") {
							GameParameter.opponentObject = hitInfo.transform.gameObject;
							makeServantToDistraction();
							GameParameter.myServant = null;
							GameParameter.opponentObject = null;
						}
					} else if (GameParameter.myServant != null && GameParameter.myServant.name == "Überzeugt") {
						if (name [0] == "DienerGegner") {
							GameParameter.opponentObject = hitInfo.transform.gameObject;
							StealServant();
							GameParameter.myServant = null;
							GameParameter.opponentObject = null;
						}
					} else if (GameParameter.myServant != null && myServantName[0] == "AngriffUndLeben") {
						if (name [0] == "Diener") {
							GameParameter.opponentObject = hitInfo.transform.gameObject;
							BuffServant();
							GameParameter.myServant = null;
							GameParameter.opponentObject = null;
						}
					} else {
						if (hitInfo.transform.gameObject == GameParameter.myServant) {
							GameParameter.myServant = null;
							colorDiener();
						} else if (name [0] == "SpecialAttack" && name[1] == "0" && GameParameter.energyCounter >= 2) {
							GameParameter.myServant = hitInfo.transform.gameObject;
							colorDiener();
						} else if (name [0] == "Diener" && name[3] == "0") {
							GameParameter.myServant = hitInfo.transform.gameObject;
							colorDiener();
						}else if (name [0] == "DienerGegner" && GameParameter.myServant != null) {
							if (isDistraction == true) {
								if (name[3] == "1") {
									GameParameter.opponentObject = hitInfo.transform.gameObject;
								} else {
									StartCoroutine(activatePanel());
								}
							} else {
								GameParameter.opponentObject = hitInfo.transform.gameObject;
							}
						} else if (name[0] == "Opponent" && GameParameter.myServant != null) {
							if (isDistraction != true) {
								GameParameter.opponentObject = hitInfo.transform.gameObject;
							} else {
								StartCoroutine(activatePanel());
							}
						}
					}
				}
			} 
			if (GameParameter.myServant != null && GameParameter.myServant.name == "SpecialAttack;0") {
				GameParameter.energyCounter -= 2;
				if (GameParameter.characterID == 4) {
					networkView.RPC("SetLifeOpponent", RPCMode.Others, new object[] {
						("" + (Int32.Parse(opponentLifePoints.text) - 2)),
						"2"
					});
					StartCoroutine(EmitSpecialAttack());
					Shake();
					opponentLifePoints.text = "" + (Int32.Parse(opponentLifePoints.text) - 2);
				}
				if (GameParameter.characterID == 2) {
					networkView.RPC("SetMyLifeOpponent", RPCMode.Others, new object[] {
						("" + (Int32.Parse(myCharacterLifePoints.text) + 2)),
						2
					});
					GameObject psObject = GameParameter.character.transform.Find("GotAttacked").gameObject;
					psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[2];
					psObject.particleSystem.Emit(1);
					myCharacterLifePoints.text = "" + (Int32.Parse(myCharacterLifePoints.text) + 2);
				}
				if (GameParameter.characterID == 1) {
					setDienerSpecial(false);
					networkView.RPC("SetDienerGegnerSpecial", RPCMode.Others, false);
				}
				if (GameParameter.characterID == 3) {
					setDienerSpecial(true);
					networkView.RPC("SetDienerGegnerSpecial", RPCMode.Others, true);
				}
				GameParameter.specialAttack.name = "SpecialAttack;" + 1;
				GameParameter.myServant = null;
				colorDiener();
			}
			if (GameParameter.myServant != null && GameParameter.opponentObject != null) {
				bool myServantDead = false;
				bool opponentServantDead = false;
				string[] servant_name = GameParameter.myServant.name.Split (';');
				string[] opponent_name = GameParameter.opponentObject.name.Split (';');
				if (opponent_name.Length <= 1) {
					networkView.RPC("SetLifeOpponent", RPCMode.Others, new object[] {
						("" + (Int32.Parse(opponentLifePoints.text) - Int32.Parse (servant_name [2]))),
						servant_name[2]
					});
					if (Int32.Parse(opponentLifePoints.text)- Int32.Parse (servant_name [2]) < 1) {
						wonOrLostText.text = "Du hast das Duell gewonnen!";
						coinsGained.text = "+300 Coins";
						levelpointsGained.text = "+50 LP";
						blackbackgroundPanel.SetActive(true);
						particleSystemPanel.SetActive(true);
						finishedPanel.SetActive(true);
						GameParameter.gameFinished = true;
						for (int i = 0; i < GameParameter.playableCardsGameObjects.Count(); i++) {
							GameParameter.playableCardsGameObjects[i].SetActive(false);
						}
						myCharacterLifePoints.font = null;//.text = "";
						opponentLifePoints.font = null;//.text = "";
						cardsOpponent.SetActive(false);
						if (gotCoins == false) {
							StartCoroutine(SendCoins (Parameter.Username, StartGame.coins + 300, StartGame.points + 50, StartGame.won+1, StartGame.lost));
							gotCoins = true;
						}
					} else {
						GameObject psObject = GameParameter.opponent.transform.Find("GotAttacked").gameObject;
						psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[Int32.Parse (servant_name [2])];
						psObject.particleSystem.Emit(1);
						opponentLifePoints.text = "" + (Int32.Parse(opponentLifePoints.text) - Int32.Parse (servant_name [2]));
						GameParameter.myServant.name = "Diener" + ";" + Int32.Parse (servant_name [1]) + ";" + servant_name [2] + ";1";
						GameParameter.myServant = null;
						GameParameter.opponentObject = null;
						colorDiener();
					}
				} else {
					if (int.Parse (opponent_name [1]) - int.Parse (servant_name [2]) <= 0) {
						opponentServantDead = true;
					} else {
						GameObject objLife = GameParameter.opponentObject.transform.Find ("Life").gameObject;
						objLife.renderer.materials [0].mainTexture = lifePoints [Int32.Parse (opponent_name [1]) - int.Parse (servant_name [2])];
						GameObject psObject = GameParameter.opponentObject.transform.Find("GotAttacked").gameObject;
						psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse ((servant_name [2]))];
						psObject.particleSystem.Emit(1);
						networkView.RPC ("SetLife", RPCMode.Others, new object[] {
							GameParameter.opponentObject.name,
							Int32.Parse (opponent_name [1]) - int.Parse (servant_name [2]),
							servant_name[2]
						});
						GameParameter.opponentObject.name = "DienerGegner" + ";" + (Int32.Parse (opponent_name [1]) - int.Parse (servant_name [2])) + ";" + opponent_name [2] + ";" + opponent_name [3];
					}
					if (int.Parse (servant_name [1]) - int.Parse (opponent_name [2]) <= 0) {
						myServantDead = true;
					} else {
						GameObject objLife = GameParameter.myServant.transform.Find ("Life").gameObject;
						objLife.renderer.materials [0].mainTexture = lifePoints [Int32.Parse (servant_name [1]) - int.Parse (opponent_name [2])];
						GameObject psObject = GameParameter.myServant.transform.Find("GotAttacked").gameObject;
						psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse ((opponent_name [2]))];
						psObject.particleSystem.Emit(1);
						networkView.RPC ("SetLife", RPCMode.Others, new object[] {
							GameParameter.myServant.name,
							Int32.Parse (servant_name [1]) - int.Parse (opponent_name [2]),
							opponent_name[2]
						});
						GameParameter.myServant.name = "Diener" + ";" + (Int32.Parse (servant_name [1]) - int.Parse (opponent_name [2])) + ";" + servant_name [2] + ";1";
					}
					if (myServantDead == true) {
						networkView.RPC ("DeleteServant", RPCMode.Others, new object[] { 
							GameParameter.myServant.name, 
							opponent_name[2] 
						});
						GameObject psObject = GameParameter.myServant.transform.Find("GotAttacked").gameObject;
						StartCoroutine(ParticleSystemEmit(GameParameter.myServant,psObject, opponent_name[2]));
						myServantDead = false;
						GameParameter.allServants.Remove (GameParameter.myServant);
						GameParameter.myServant.name = "tot";
					}
					if (opponentServantDead == true) {
						networkView.RPC ("DeleteServant", RPCMode.Others, new object[] { 
							GameParameter.opponentObject.name,
							servant_name[2]
						});
						GameObject psObject = GameParameter.opponentObject.transform.Find("GotAttacked").gameObject;
						StartCoroutine(ParticleSystemEmit(GameParameter.opponentObject,psObject, servant_name[2]));
						GameParameter.allServantsOpponent.Remove (GameParameter.opponentObject);
						GameParameter.opponentObject.name = "tot";
						opponentServantDead = false;
					}
					GameParameter.myServant = null;
					GameParameter.opponentObject = null;
					colorDiener();
				}
			}
		}
	}
	public void Shake(){
		originPosition = transform.position;
		originRotation = transform.rotation;
		shakeIntensity = .1f;
		shakeDecay = 0.002f;
	}
	public IEnumerator EmitSpecialAttack() {
		GameObject psObject = GameParameter.characterSpecialAttack.transform.Find("Fireball").gameObject;
		psObject.particleSystem.Emit(1);
		yield return new WaitForSeconds (1.1f);
		psObject = GameParameter.opponent.transform.Find("GotAttacked").gameObject;
		psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[2];
		psObject.particleSystem.Emit(1);
	}
	public IEnumerator ParticleSystemEmit(GameObject which, GameObject psObject, string newAttack) {
		Debug.Log ("Sub: "+newAttack);
		if (newAttack != "100") {
			psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse (newAttack)];
			psObject.particleSystem.Emit(1);
			yield return new WaitForSeconds (1.5f);
		}
		psObject = which.transform.Find("X").gameObject;
		psObject.particleSystem.Emit(1);
		yield return new WaitForSeconds (1f);
		which.transform.position += new Vector3 (1500f, 0, 0);
		HighlightPlayableCard.setGegnerDienerPosition ();
		HighlightPlayableCard.setDienerPosition ();
	}
	public IEnumerator SendCoins(string username, int c, int poins, int won, int lost) {
		string saveUrl = "http://digitalcardgame.bplaced.net/save_coins.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("coins", c);
		form.AddField ("poins", poins);
		form.AddField ("won", won);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
		} else {
			if (www.text != "False") {
				Debug.Log("Gespeichert");
				Parameter.Coins = c;
				Parameter.Levelpoints = poins;
			}
			www.Dispose ();
		}
	}
	public IEnumerator activatePanel(){
		attackDistractionBeforeAnythingOtherText.text = "Du kannst den Gegner nicht angreifen, solange er eine Ablenkung eingesetzt hat.";
		attackDistractionBeforeAnythingOtherPanel.SetActive (true);
		attackDistractionBeforeAnythingOtherPanel.GetComponent<Animator>().Play("NotificationSlideIn");
		yield return new WaitForSeconds(3.2f);
		attackDistractionBeforeAnythingOtherPanel.SetActive (false);
	}
	public bool checkForDistraction() {
		bool isDestraction = false;
		foreach (var servant in GameParameter.allServantsOpponent) {
			string[] name = servant.name.Split(';');
			if (name[3] == "1") {
				isDestraction = true;
			}
		}
		return isDestraction;
	}
	public static void colorDiener() {
		GameObject psObject;
		foreach (GameObject servant in GameParameter.allServants) {
			psObject = servant.transform.Find("Zzz").gameObject;
			string[] name = servant.name.Split(';');
			if (GameParameter.myServant != null && servant == GameParameter.myServant) {
				servant.transform.Find("Main").renderer.materials[1].color = Color.green;
			} else {
				if (name[3] == "1") {
					//servant.transform.Find("Main").renderer.materials[1].color = new Color(1f,0.6f,0.6f,1f);
					servant.transform.Find("Main").renderer.materials[1].color = Color.white;
					psObject.particleSystem.Play();
					psObject.SetActive(true);
				} else {
					servant.transform.Find("Main").renderer.materials[1].color = Color.white;
					psObject.particleSystem.Stop();
					psObject.SetActive(false);
				}
			}
		}
		/*foreach (var item in GameParameter.allServantsOpponent) {
Debug.Log (item.GetComponent<Servant>().isFirstRound);
if (!item.GetComponent<Servant>().isFirstRound) {
psObject = item.transform.Find("Zzz").gameObject;
psObject.particleSystem.Stop();
psObject.SetActive(false);
}
}*/
		if (GameParameter.myServant != null && GameParameter.myServant.name == "SpecialAttack;0") {
			GameParameter.myServant.renderer.materials [1].color = Color.green;
		} else {
			GameParameter.specialAttack.renderer.materials [1].color = Color.white;
		}
	}
	public void setDienerSpecial(bool isServant) {
		GameObject servant = null;
		if (isServant == false) {
			servant = (GameObject)Instantiate (GameParameter.Diener, new Vector3 (0f, 0f, 0f), Quaternion.Euler (0, 0, 0));
			servant.name = "Diener;" + 2 + ";" + 2 + ";1";
			GameObject objAttack = servant.transform.Find("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = attackPoints[2];
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[2];
			GameObject objCharacterDesign = servant.transform.Find("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = GameParameter.playedServantsDesign[1];
		} else {
			servant = (GameObject)Instantiate(GameParameter.Distraction,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
			servant.name = "Diener;" + 1 + ";" + 1 + ";1";
			GameObject objAttack = servant.transform.Find("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = attackPoints[1];
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[1];
			GameObject objCharacterDesign = servant.transform.Find("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = GameParameter.playedServantsDesign[3];
		}
		servant.AddComponent<BoxCollider>();
		servant.transform.localScale = new Vector3(100,100,100);
		servant.transform.Rotate(-90f,-180f,0);
		GameParameter.allServants.Add (servant);
		HighlightPlayableCard.setDienerPosition ();
	}
	[RPC]
	public void SetDienerGegnerSpecial(bool isServant) {
		GameObject servant = null;
		if (isServant == false) {
			servant = (GameObject)Instantiate (GameParameter.Diener, new Vector3 (0f, 0f, 0f), Quaternion.Euler (0, 0, 0));
			servant.name = "DienerGegner;" + 2 + ";" + 2 + ";0";
			GameObject objAttack = servant.transform.Find("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = attackPoints[2];
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[2];
			GameObject objCharacterDesign = servant.transform.Find("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = GameParameter.playedServantsDesign[1];
		} else {
			servant = (GameObject)Instantiate(GameParameter.Distraction,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
			servant.name = "DienerGegner;" + 1 + ";" + 1 + ";1";
			GameObject objAttack = servant.transform.Find("Attack").gameObject;
			objAttack.renderer.materials [0].mainTexture = attackPoints[1];
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[1];
			GameObject objCharacterDesign = servant.transform.Find("Main").gameObject;
			objCharacterDesign.renderer.materials [1].mainTexture = GameParameter.playedServantsDesign[3];
		}
		servant.AddComponent<BoxCollider>();
		servant.transform.localScale = new Vector3(100,100,100);
		servant.transform.Rotate(-90f,-180f,0);
		GameParameter.allServantsOpponent.Add (servant);
		HighlightPlayableCard.setGegnerDienerPosition ();
	}
	[RPC]
	public void SetLifeOpponent(string newLifePoints, string damage) {
		if (Int32.Parse(newLifePoints) > 0) {
			GameObject psObject = GameParameter.character.transform.Find("GotAttacked").gameObject;
			psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[Int32.Parse(damage)];
			psObject.particleSystem.Emit(1);
			myCharacterLifePoints.text = newLifePoints;
			Shake();
		} else {
			GameParameter.gameFinished = true;
			wonOrLostText.text = "Du hast das Duell verloren!";
			coinsGained.text = "+30 Coins";
			levelpointsGained.text = "+5 LP";
			blackbackgroundPanel.SetActive(true);
			finishedPanel.SetActive(true);
			for (int i = 0; i < GameParameter.playableCardsGameObjects.Count(); i++) {
				GameParameter.playableCardsGameObjects[i].SetActive(false);
			}
			if (gotCoins == false) {
				Shake();
				StartCoroutine(SendCoins (Parameter.Username, StartGame.coins + 30, StartGame.points + 5, StartGame.won, StartGame.lost+1));
				gotCoins = true;
			}
			myCharacterLifePoints.font = null;//.text = "";
			//myCharacterLifePoints.text = "";
			opponentLifePoints.font = null;//.text = "";
			cardsOpponent.SetActive(false);
		}
	}
	[RPC]
	public void SetMyLifeOpponent(string newLifePoints, int damage) {
		if (int.Parse(newLifePoints) < int.Parse(opponentLifePoints.text)) {
			Shake();
		}
		opponentLifePoints.text = newLifePoints;
		GameObject psObject = GameParameter.opponent.transform.Find("GotAttacked").gameObject;
		psObject.particleSystem.renderer.material.mainTexture = lifePointsKaPoom[damage];
		psObject.particleSystem.Emit(1);
	}
	[RPC]
	public void DeleteServant(string who, string substractAttackPoints) {
		string[] name = who.Split (';');
		if (name[0] == "Diener") {
			GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
			if (servant == null) {
				servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
			}
			StartCoroutine(DeleteServantParticleSystem(servant, substractAttackPoints, GameParameter.allServantsOpponent));
		} else {
			GameObject servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";0");
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";1");
			}
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]);
			}
			StartCoroutine(DeleteServantParticleSystem(servant, substractAttackPoints, GameParameter.allServants));
		}
	}
	public IEnumerator DeleteServantParticleSystem(GameObject servant, string substractAttackPoints, List<GameObject> servants) {
		GameObject psObject = servant.transform.Find("GotAttacked").gameObject;
		if (substractAttackPoints != "100") {
			psObject.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse (substractAttackPoints)];
			psObject.particleSystem.Emit(1);
			yield return new WaitForSeconds (1.5f);
		}
		psObject = servant.transform.Find("X").gameObject;
		psObject.particleSystem.Emit(1);
		yield return new WaitForSeconds (1f);
		servant.transform.position += new Vector3(1500f,0,0);
		servant.name = "tot";
		servants.Remove(servant);
		HighlightPlayableCard.setDienerPosition ();
		HighlightPlayableCard.setGegnerDienerPosition ();
	}
	[RPC]
	public void SetLife(string who, int newLife, string substractAttackPoints) {
		Debug.Log (newLife);
		string[] name = who.Split (';');
		if (name[0] == "Diener") {
			GameObject servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";1");
			if (servant == null) {
				servant = GameObject.Find("DienerGegner;"+name[1]+";"+name[2]+";0");
			} 
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[newLife];
			string[] nameServant = servant.name.Split(';');
			GameObject psObject = servant.transform.Find("Zzz").gameObject;
			psObject.particleSystem.Play();
			psObject.SetActive(true);
			GameObject psObjectServant = servant.transform.Find("GotAttacked").gameObject;
			psObjectServant.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse (substractAttackPoints)];
			psObjectServant.particleSystem.Emit(1);
			servant.name = "DienerGegner" +";"+ newLife +";"+ name[2]+";"+nameServant[3];
		} else {
			GameObject servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";0");
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]+";1");
			}
			if (servant == null) {
				servant = GameObject.Find("Diener;"+name[1]+";"+name[2]);
			}
			GameObject objLife = servant.transform.Find("Life").gameObject;
			objLife.renderer.materials [0].mainTexture = lifePoints[newLife];
			GameObject psObjectServant = servant.transform.Find("GotAttacked").gameObject;
			psObjectServant.particleSystem.renderer.material.mainTexture = attackPointsKaPoom[int.Parse (substractAttackPoints)];
			psObjectServant.particleSystem.Emit(1);
			servant.name = "Diener" +";"+ newLife +";"+ name[2]+";0";
		}
	}
	public void makeServantToDistraction() {
		int servantCounter = 0;
		for (int i = 0; i < GameParameter.allServants.Count; i++) {
			if (GameParameter.allServants[i] == GameParameter.opponentObject) {
				servantCounter = i;
			}
		}
		GameObject objCharacterDesign = GameParameter.opponentObject.transform.Find ("Main").gameObject;
		Texture texture = objCharacterDesign.renderer.materials [1].mainTexture;
		int charType = 0;
		for (int i = 0; i < GameParameter.playedServantsDesign.Length; i++) {
			if (GameParameter.playedServantsDesign[i].name == texture.name) {
				charType = i;
			}
		}
		String[] nameOpp = GameParameter.opponentObject.name.Split(';');
		GameObject servant = (GameObject)Instantiate(GameParameter.Distraction,new Vector3(0f,0f,0f),Quaternion.Euler(0,0,0));
		servant.name = GameParameter.opponentObject.name;
		servant.AddComponent<BoxCollider>();
		servant.transform.localScale = new Vector3(100,100,100);
		servant.transform.Rotate(-90f,-180f,0);
		GameObject objCharacterDesign1 = servant.transform.Find ("Main").gameObject;
		GameObject objCharacterDesignOld = GameParameter.opponentObject.transform.Find ("Main").gameObject;
		objCharacterDesign1.renderer.materials [1].mainTexture = objCharacterDesignOld.renderer.materials [1].mainTexture;
		GameObject objAttack = servant.transform.Find ("Attack").gameObject;
		GameObject objAttackOld = GameParameter.opponentObject.transform.Find ("Attack").gameObject;
		objAttack.renderer.materials [0].mainTexture = objAttackOld.renderer.materials [0].mainTexture;
		GameObject objLife = servant.transform.Find ("Life").gameObject;
		GameObject objLifeOld = GameParameter.opponentObject.transform.Find ("Life").gameObject;
		objLife.renderer.materials [0].mainTexture = objLifeOld.renderer.materials [0].mainTexture;
		GameParameter.allServants[servantCounter] = servant;
		HighlightPlayableCard.setDienerPosition();
		GameParameter.opponentObject.name = "tot";
		GameParameter.opponentObject.transform.position += new Vector3(1500f,0,0);
		networkView.RPC ("makeServantToDistractionOpponent", RPCMode.Others, new object[] {
			nameOpp[1],
			nameOpp[2],
			charType
		});
	}
	[RPC]
	public void makeServantToDistractionOpponent(string life, string attack, int chartype) {
		int servantCounter = 0;
		GameObject oldServant = null;
		for (int i = 0; i < GameParameter.allServantsOpponent.Count; i++) {
			Debug.Log(GameParameter.allServantsOpponent[i].name +" : "+ "DienerGegner;" + life + ";" + attack + ";0");
			if (GameParameter.allServantsOpponent[i].name == "DienerGegner;" + life + ";" + attack + ";0" ) {
				servantCounter = i;
				oldServant = GameParameter.allServantsOpponent[i];
			}
		}
		GameObject servant = (GameObject)Instantiate (GameParameter.Distraction, new Vector3 (0f, 0f, 0f), Quaternion.Euler (0, 0, 0));
		servant.name = "DienerGegner;" + life + ";" + attack + ";1";
		servant.AddComponent<BoxCollider> ();
		servant.transform.localScale = new Vector3 (100, 100, 100);
		servant.transform.Rotate (-90f, -180f, 0);
		servant.AddComponent<Servant> ();
		GameObject objCharacterDesign = servant.transform.Find ("Main").gameObject;
		GameObject objCharacterDesignOld = oldServant.transform.Find ("Main").gameObject;
		objCharacterDesign.renderer.materials [1].mainTexture = objCharacterDesignOld.renderer.materials [1].mainTexture;
		GameObject objAttack = servant.transform.Find ("Attack").gameObject;
		objAttack.renderer.materials [0].mainTexture = attackPoints [Int32.Parse (attack)];
		GameObject objLife = servant.transform.Find ("Life").gameObject;
		objLife.renderer.materials [0].mainTexture = lifePoints [Int32.Parse (life)];
		GameParameter.allServantsOpponent[servantCounter] = servant;
		HighlightPlayableCard.setGegnerDienerPosition ();
		oldServant.name = "tot";
		oldServant.transform.position += new Vector3(1500f,0,0);
	}
	public void StealServant() {
		String[] nameOpp = GameParameter.opponentObject.name.Split (';');
		networkView.RPC ("StealServantOpponent", RPCMode.Others, new object[] {
			GameParameter.opponentObject.name
		});
		GameParameter.opponentObject.name = "Diener;" + nameOpp[1] + ";" + nameOpp[2] + ";1";
		GameParameter.allServantsOpponent.Remove (GameParameter.opponentObject);
		GameParameter.allServants.Add (GameParameter.opponentObject);
		HighlightPlayableCard.setDienerPosition ();
		HighlightPlayableCard.setGegnerDienerPosition ();
	}
	[RPC]
	public void StealServantOpponent(string name) {
		String[] nameOpp = name.Split(';');
		GameObject servant = GameObject.Find("Diener;"+nameOpp[1]+";"+nameOpp[2]+";1");
		if (servant == null) {
			servant = GameObject.Find("Diener;"+nameOpp[1]+";"+nameOpp[2]+";0");
		}
		servant.name = "DienerGegner;" + nameOpp[1]+";"+nameOpp[2]+";"+nameOpp[3];
		GameParameter.allServantsOpponent.Add (servant);
		GameParameter.allServants.Remove (servant);
		HighlightPlayableCard.setDienerPosition ();
		HighlightPlayableCard.setGegnerDienerPosition ();
	}
	public void BuffServant() {
		String[] nameOpp = GameParameter.opponentObject.name.Split (';');
		String[] nameAdd = GameParameter.myServant.name.Split (';');
		networkView.RPC ("BuffServantOpponent", RPCMode.Others, new object[] {
			GameParameter.opponentObject.name,
			GameParameter.myServant.name
		});
		int addAttack = Int32.Parse (nameOpp [2]) + Int32.Parse (nameAdd [1]);
		int addLife = Int32.Parse (nameOpp [1]) + Int32.Parse (nameAdd [1]);
		if (addAttack > 10) {
			addAttack = 10;
		}
		if (addLife > 10) {
			addLife = 10;
		}
		GameObject objAttack = GameParameter.opponentObject.transform.Find ("Attack").gameObject;
		objAttack.renderer.materials [0].mainTexture = attackPoints [addAttack];
		GameObject objLife = GameParameter.opponentObject.transform.Find ("Life").gameObject;
		objLife.renderer.materials [0].mainTexture = lifePoints [addLife];
		GameParameter.opponentObject.name = nameOpp [0] + ";" + addLife + ";" + addAttack + ";" + nameOpp [3];
	}
	[RPC]
	public void BuffServantOpponent(string name, string add) {
		String[] nameOpp = name.Split (';');
		String[] nameAdd = add.Split (';');
		bool check = false;
		GameObject servant = GameObject.Find("DienerGegner;"+nameOpp[1]+";"+nameOpp[2]+";1");
		if (servant == null) {
			servant = GameObject.Find("DienerGegner;"+nameOpp[1]+";"+nameOpp[2]+";0");
			check = true;
		}
		int addAttack = Int32.Parse (nameOpp [2]) + Int32.Parse (nameAdd [1]);
		int addLife = Int32.Parse (nameOpp [1]) + Int32.Parse (nameAdd [1]);
		if (addAttack > 10) {
			addAttack = 10;
		}
		if (addLife > 10) {
			addLife = 10;
		}
		GameObject objAttack = servant.transform.Find ("Attack").gameObject;
		objAttack.renderer.materials [0].mainTexture = attackPoints [addAttack];
		GameObject objLife = servant.transform.Find ("Life").gameObject;
		objLife.renderer.materials [0].mainTexture = lifePoints [addLife];
		if (check) {
			servant.name = "DienerGegner" + ";" + addLife + ";" + addAttack + ";0";
		} else {
			servant.name = "DienerGegner" + ";" + addLife + ";" + addAttack + ";1";
		}
	}
}