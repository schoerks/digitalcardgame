﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpriteAnimator : MonoBehaviour {

	public Sprite[] sprites;
	public float framesPerSecond;

	//private SpriteRenderer spriteRenderer;


	// Use this for initialization
	void Start () {
		//spriteRenderer = renderer as SpriteRenderer;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("Change");
		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % sprites.Length;
		this.GetComponent<Image> ().sprite = sprites [index];
	}


}
