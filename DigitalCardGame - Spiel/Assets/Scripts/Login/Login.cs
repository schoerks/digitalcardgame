﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using System.Linq.Expressions;

public class Login : MonoBehaviour {
	
	public InputField username;
	public InputField password;
	
	EventSystem system;
	
	public GameObject waitingScreen;
	public GameObject waitingScreenButton;
	public Text waitingScreenText;
	
	public Text cancelButtonText;
	
	public GameObject loadAnimation;
	public Image loadImage;
	
	void Start () {
		system = EventSystem.current;
		waitingScreenButton.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		loadImage.transform.Rotate(0.0f, 0.0f, -1.0f);
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			
			if (next!= null) {
				
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(system));
				
				system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
			}
		}/*
		if (Input.GetKeyDown(KeyCode.P))
		{
			Application.LoadLevel("Presentation");
		}*/
	}
	
	public void CheckLoginData () {
		if (username.text == "" && password.text == "") {
			username.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
			password.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
		} else if (username.text == "" && password.text != "") {
			username.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
		} else if (username.text != "" && password.text == "")
			password.placeholder.color = new Color (1f, 0f, 0f , 0.7f);
		else {
			waitingScreenButton.SetActive(false);
			waitingScreen.SetActive(true);
			loadAnimation.SetActive (true);
			StartCoroutine(SendData (username.text, password.text));
		}
	}
	
	public IEnumerator SendData(string username, string password) {
		string saveUrl = "http://digitalcardgame.bplaced.net/login.php";
		WWWForm form = new WWWForm ();
		form.AddField ("username", username);
		form.AddField ("password", password);
		WWW www = new WWW (saveUrl, form);
		yield return www;
		if (www.error != null) {
			Debug.Log ("Failed! " + www.error);
			waitingScreenText.text = "Eingabedaten falsch!";
			cancelButtonText.text = "Erneut versuchen";
			waitingScreenButton.SetActive(true);
			loadAnimation.SetActive (false);
		} else {
			if (www.text != "false") {
				Parameter.Username = username;
				Parameter.myCards = www.text.Split(',').ToList();
				Application.LoadLevel ("MainMenu");
			} else {
				if (username == "Error1") {
					Debug.Log ("Failed to login!");
					waitingScreenText.text = "Eingabedaten falsch!";
					cancelButtonText.text = "Ok";
					waitingScreenButton.SetActive(true);
					loadAnimation.SetActive (false);
				}
				waitingScreenText.text = "Eingabedaten falsch!";
				cancelButtonText.text = "Ok";
				waitingScreenButton.SetActive(true);
				loadAnimation.SetActive (false);
			}
			www.Dispose ();
		}
		username = "";
		password = "";
	}
	
	public void ClickedCancelOrTryAgain() {
		waitingScreen.SetActive (false);
		loadAnimation.SetActive (false);
		waitingScreenText.text = "Bitte warten...";
		cancelButtonText.text = "Abbrechen";
	}
	
	public void EndGame () {
		Application.Quit ();
	}
	
	public void RegistrationClicked () {
		StartCoroutine (LoadRegisterScene());
	}

	public IEnumerator LoadRegisterScene() {
		yield return new WaitForSeconds (0.3f);
		Application.LoadLevel ("Registration");
	}
}