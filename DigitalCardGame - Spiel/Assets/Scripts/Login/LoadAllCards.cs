﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class LoadAllCards : MonoBehaviour {
	public static List<Card> allCards;
	// Use this for initialization
	void Start () {
		StartCoroutine( SendData ());
	}
	// Update is called once per frame
	void Update () {
	}
	/// <summary>
	/// Load all cards from database
	/// </summary>
	/// <returns>The data.</returns>
	public IEnumerator SendData() {
		string saveUrl = "http://digitalcardgame.bplaced.net/getCards.php";
		WWWForm form = new WWWForm ();
		form.AddField ("char", 0);
		WWW www = new WWW(saveUrl, form);
		yield return www;
		if (www.error != null) {
			print(www.error);
		} else {
			Split_Cards(www.text);
		}
	}
	/// <summary>
	/// Splits the cards.
	/// </summary>
	/// <param name="text">Text.</param>
	public void Split_Cards(string text) {
		allCards = new List<Card> ();
		string[] line = text.Split ('\n');
		for (int i = 0; i < line.Length-1; i++) {
			string[] act = line [i].Split (';'); 
			Card card = gameObject.AddComponent<Card>();
			card.Id = System.Int32.Parse (act [0]);
			string x = CheckText (act[1]);
			card.Text = x;
			card.Leben = System.Int32.Parse (act [2]);
			card.Angriff = System.Int32.Parse (act [3]);
			card.Energy = System.Int32.Parse (act [4]);
			card.Character = System.Int32.Parse (act [5]);
			card.Selected = false;
			if (card.Leben == 0 && card.Angriff == 0) {
				card.IsSpell = true;
			}
			allCards.Add (card);
		}
		Parameter.allCards = allCards;
		Parameter.allCardsForShop = allCards;
	}
	/// <summary>
	/// Ersetzt Umlaute
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	public string CheckText (string x) {
		if (x.Contains("ue")) {
			x = x.Replace ("ue", "ü");
		} 
		if (x.Contains("ae")) {
			x = x.Replace ("ae", "ä");
		}
		if (x.Contains("oe")) {
			x = x.Replace ("oe", "ö");
		}
		if (x.Contains("Ue")) {
			x = x.Replace ("Ue", "Ü");
		}
		if (x.Contains("Ae")) {
			x = x.Replace ("Ae", "Ä");
		}
		if (x.Contains("Oe")) {
			x = x.Replace ("Oe", "Ö");
		}
		return x;
	}
}